package edu.upc.fib.knitter.usecase;

import android.content.Context;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import edu.upc.fib.knitter.R;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RegisterTest {

    @Mock Context mMockContext;

    private static final String PHONE = "600123123";
    private static final String SALT = "LwigmrmPIV";
    private static final String SHA_ANSWER = "C066467A106C111886E4075434D3535F300BFDEB8FB61AAC23631068615D445E";


    @Test
    public void getPhone() {
        when(mMockContext.getString(R.string.salt)).thenReturn(SALT);
        Register r = new Register(PHONE, mMockContext);
        String result = r.getPhone();
        assertEquals(PHONE, result);
    }

    @Test
    public void getHash() {
        when(mMockContext.getString(R.string.salt)).thenReturn(SALT);
        Register r = new Register(PHONE, mMockContext);
        String result = r.getHash();
        assertEquals(SHA_ANSWER,result);
    }

}