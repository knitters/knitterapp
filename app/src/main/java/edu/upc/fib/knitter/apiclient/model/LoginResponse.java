package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class LoginResponse implements Serializable {

    @SerializedName("token") String token;
    @SerializedName("message") String message;
    @SerializedName("wasDisabled") Boolean wasDisabled;

    public LoginResponse(String token, String message, Boolean wasDisabled) {
        this.token = token;
        this.message = message;
        this.wasDisabled = wasDisabled;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getWasDisabled() {
        return wasDisabled;
    }

    public void setWasDisabled(Boolean wasDisabled) {
        this.wasDisabled = wasDisabled;
    }

}
