package edu.upc.fib.knitter.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.ui.presenter.DemandsPresenter;
import edu.upc.fib.knitter.ui.presenter.UserDemandsPresenter;

public class UserDemandsViewHolder extends RecyclerView.ViewHolder {

    private final UserDemandsPresenter presenter;
    @BindView(R.id.tv_petition_name) TextView nameTextView;
    @BindView(R.id.tv_petition_author) TextView authorTextView;
    @BindView(R.id.info) RelativeLayout layout;
    private int hei, wid;

    public UserDemandsViewHolder(View itemView, UserDemandsPresenter presenter, int numberElements) {
        super(itemView);
        hei = (itemView.getResources().getDisplayMetrics().heightPixels - 280) / numberElements;
        wid = itemView.getResources().getDisplayMetrics().widthPixels;
        this.presenter = presenter;
        ButterKnife.bind(this, itemView);
    }

    public void render(DemandUser petition, String userName) {
        hookListeners(petition);
        layout.setLayoutParams(new RelativeLayout.LayoutParams(wid,hei));
        renderOfferName(petition.getTitle());
        //renderOfferAuthor(petition.getAuthor().getFirstName());
        renderOfferAuthor(userName);
    }

    private void hookListeners(final DemandUser demand) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                presenter.onDemandClicked(demand);
            }
        });
    }

    private void renderOfferName(String name) {
        nameTextView.setText(name);
    }

    private void renderOfferAuthor(String author) {
        authorTextView.setText(author);
    }
}