package edu.upc.fib.knitter.Chat.Model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.ImageViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import edu.upc.fib.knitter.R;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.ViewHolder> {

    public static final int MSG_TYPE_LEFT = 0;
    public static final int MSG_TYPE_RIGHT = 1;
    private Context context;
    private List<Requests> lRequests;
    private String imageUrl;

    FirebaseUser firebaseUser;

    public RequestsAdapter(Context context, List<Requests> lRequests){
        this.context = context;
        this.lRequests = lRequests;
        this.imageUrl = imageUrl;
    }


    @NonNull
    @Override
    public RequestsAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if(i == MSG_TYPE_RIGHT){
            View view = LayoutInflater.from(context).inflate(R.layout.chat_item_right, viewGroup, false);
            return new RequestsAdapter.ViewHolder(view);
        }
        else {
            View view = LayoutInflater.from(context).inflate(R.layout.chat_item_left, viewGroup, false);
            return new RequestsAdapter.ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RequestsAdapter.ViewHolder viewHolder, int i) {
        Requests request = lRequests.get(i);
        String text = request.getName();

        viewHolder.show_message.setText(text);

        if(imageUrl.equals("default")){
            //viewHolder.profile_image.setImageResource(R.drawable.ic_grandmother);
        }
        else{
            //TODO set users image
        }
    }

    @Override
    public int getItemCount() {
        return lRequests.size();
    }

    @Override
    public int getItemViewType(int position) {
        //TODO return offer o petition
        /*
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if(lRequests.get(position).getSender().equals(firebaseUser.getUid())) {
            return MSG_TYPE_RIGHT;
        }
        else return MSG_TYPE_LEFT;
        */
        return 1;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView show_message;
        public ImageViewCompat profile_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            show_message = itemView.findViewById(R.id.chat_message);
        }


    }
}
