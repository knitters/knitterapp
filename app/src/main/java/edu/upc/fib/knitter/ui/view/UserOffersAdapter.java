package edu.upc.fib.knitter.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.apiclient.model.OfferUser;
import edu.upc.fib.knitter.ui.presenter.OffersPresenter;
import edu.upc.fib.knitter.ui.presenter.UserOffersPresenter;

class UserOffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final UserOffersPresenter presenter;
    private final List<OfferUser> offers;
    private int numberElements;
    private String userName;

    public UserOffersAdapter(UserOffersPresenter presenter, String userName) {
        this.presenter = presenter;
        this.offers = new ArrayList<>();
    }

    void addAll(List<OfferUser> collection, int nElem) {
        offers.clear();
        offers.addAll(collection);
        this.numberElements = nElem;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_row, parent, false);
        return new UserOffersViewHolder(view, presenter, numberElements);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserOffersViewHolder offersViewHolder = (UserOffersViewHolder) holder;
        OfferUser offer = offers.get(position);
        offersViewHolder.render(offer, userName);
    }

    @Override public int getItemCount() {
        return offers.size();
    }
}