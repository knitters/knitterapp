package edu.upc.fib.knitter.Chat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


import java.util.HashMap;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.ui.view.BaseActivity;
import edu.upc.fib.knitter.Chat.Model.ChatUser;

public class ChatLoginActivity extends BaseActivity {


    private FirebaseAuth auth;
    private DatabaseReference dbReference;
    private ChatUser chatUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        auth = FirebaseAuth.getInstance();
        getUserData();
        //doRegister();
        String id = chatUser.getPhone() + "@knitters.com";
        String password = "asdf1234";
        login(id, password);
    }

    private void getUserData(){
        SharedPreferences mPrefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        //SharedPreferences.Editor mEditor = mPrefs.edit();
        String phone, name, imagePath;
        phone = mPrefs.getString("id","");
        name = mPrefs.getString("firstName", "ANONIMO");
        imagePath = mPrefs.getString("imagePath", "default");
        chatUser = new ChatUser();
        chatUser.setName(name);
        chatUser.setPhone(phone);
        chatUser.setImage(imagePath);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = auth.getCurrentUser();
    }

    private void doRegister(){
        //ChatRegister chatRegister = new ChatRegister();
        //chatRegister.register("test", "id","pwd");
        String username, id, imagePath;
        username = chatUser.getName();
        if(username.equals("")) username = "ANONIMO";
        id = chatUser.getPhone() + "@knitters.com";
        imagePath = chatUser.getImage();
        register(username, id,"asdf1234",imagePath);
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_splash;
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, ChatLoginActivity.class);
        context.startActivity(intent);
    }

    public void login(final String id, final String password){
        //TODO refine register and add loading progress
        auth.signInWithEmailAndPassword(id,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                FirebaseAuth auth = FirebaseAuth.getInstance();
                if(task.isSuccessful() && auth.getCurrentUser() != null){
                    goToMain();
                }
                else register(chatUser.getName(), id, password, "default");
            }

        });

    }


    public void register(final String username, String id, String password, final String imagePath){
        auth.createUserWithEmailAndPassword(id, password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()) {

                            FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                            String uid = currentUser.getUid();
                            dbReference = FirebaseDatabase.getInstance().getReference().child("Users").child(uid);
                            HashMap<String, String> userMap = new HashMap<>();
                            userMap.put("name", username);
                            if(!imagePath.equals("")) userMap.put("image","default");
                            else userMap.put("image","default");
                            userMap.put("phone", chatUser.getPhone());
                            dbReference.setValue(userMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    goToMain();
                                }
                            });

                        }
                        else {
                            Toast.makeText(ChatLoginActivity.this, "FAILED", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    private void goToMain(){
        Intent intent = new Intent(ChatLoginActivity.this, ChatMainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

}
