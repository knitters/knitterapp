package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.GetOffersResponse;
import retrofit2.Call;
import retrofit2.Response;

public class GetOffers {

    private final ApiRepository repository;

    @Inject
    public GetOffers(ApiRepository repository) {
        this.repository = repository;
    }

    public void getOffers(final int numElements, final String prevCursor, final String nextCursor, final Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<GetOffersResponse> response = repository.getOffers(numElements, prevCursor, nextCursor);
                response.enqueue(new retrofit2.Callback<GetOffersResponse>() {
                    @Override
                    public void onResponse(Call<GetOffersResponse> call, Response<GetOffersResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<GetOffersResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    //public void getUserOffers(final Callback callback) {
    //    new Thread(new Runnable() {
    //        @Override public void run() {
    //            Call<GetOffersResponse> response = repository.getUserOffers();
    //            response.enqueue(new retrofit2.Callback<GetOffersResponse>() {
    //                @Override
    //                public void onResponse(Call<GetOffersResponse> call, Response<GetOffersResponse> response) {
    //                    callback.onResponse(call, response);
    //                }
    //
    //                @Override
    //                public void onFailure(Call<GetOffersResponse> call, Throwable t) {
    //                    callback.onFailure(call, t);
    //                }
    //            });
    //        }
    //    }).start();
    //}

    public interface Callback {
        void onResponse(Call<GetOffersResponse> call, Response<GetOffersResponse> response);
        void onFailure(Call<GetOffersResponse> call, Throwable t);
    }
}