package edu.upc.fib.knitter.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.ui.presenter.DemandsPresenter;

class DemandsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final DemandsPresenter presenter;
    private final List<Demand> demands;
    private int numberElements;

    public DemandsAdapter(DemandsPresenter presenter) {
        this.presenter = presenter;
        this.demands = new ArrayList<>();
    }

    void addAll(Collection<Demand> collection, int nElem) {
        demands.clear();
        demands.addAll(collection);
        this.numberElements = nElem;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.demand_row, parent, false);
        return new DemandsViewHolder(view, presenter, numberElements);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        DemandsViewHolder demandsViewHolder = (DemandsViewHolder) holder;
        Demand demand = demands.get(position);
        demandsViewHolder.render(demand);
    }

    @Override public int getItemCount() {
        return demands.size();
    }
}