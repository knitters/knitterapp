package edu.upc.fib.knitter.ui.presenter;

import android.util.Log;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.usecase.GetUser;
import edu.upc.fib.knitter.usecase.UserProfile;
import retrofit2.Call;
import retrofit2.Response;

public class MainMenuPresenter extends Presenter<MainMenuPresenter.View> {

    private final UserProfile userProfile;

    @Inject
    public MainMenuPresenter(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override public void initialize() {
        super.initialize();
        getUser();
    }

    public void onOfferSearchClick() {
        getView().openOfferSearch();
    }

    public void onDemandSearchClick() {
        getView().openDemandSearch();
    }

    public void onMyOfferSearchClick() {
        getView().openMyOfferSearch();
    }

    public void onMyDemandSearchClick() {
        getView().openMyDemandSearch();
    }

    public void onPendingClick(){ getView().openPendingDemands();}

    public void onOfferPublishClick(){
        getView().openOfferPublish();
    }

    public void onDemandPublishClick(){
        getView().openDemandPublish();
    }

    public void onUserClick() {
        getView().openUser();
    }

    public void onChatClick(){ getView().openChat();}

    public void getUser() {
        userProfile.getProfile(new UserProfile.Callback() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    getView().saveUserInfo(response.body());
                    getView().setUser(response.body());
                } else {
                    // error response, no access to resource?
                    getView().showMessage("No se ha podido obtener la información del usuario");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());

            }
        });
    }

    public interface View extends Presenter.View {
        void openMyOfferSearch();
        void openOfferSearch();
        void openOfferPublish();
        void openDemandSearch();
        void openMyDemandSearch();
        void openDemandPublish();
        void openUser();
        void openChat();
        void setUser(User user);
        void openPendingDemands();
        void showMessage(String message);
        void saveUserInfo(User response);
    }

}
