package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class Demand {

    @SerializedName("id") String id;
    @SerializedName("title") private String title;
    @SerializedName("description") private String description;
    @SerializedName("author") User author;
    @SerializedName("createdAt") String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
