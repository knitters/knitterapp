package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDemandsUserResponse {

    @SerializedName("demands") private List<DemandUser> demands;
    @SerializedName("metadata") private PaginationMetaData metaData;

    public List<DemandUser> getDemands() {
        return demands;
    }

    public void setDemands(List<DemandUser> demands) {
        this.demands = demands;
    }

    public PaginationMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(PaginationMetaData metaData) {
        this.metaData = metaData;
    }
}
