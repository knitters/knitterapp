package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetDemandsResponse {

    @SerializedName("demands") private List<Demand> demands;
    @SerializedName("metadata") private PaginationMetaData metaData;

    public List<Demand> getDemands() {
        return demands;
    }

    public void setDemands(List<Demand> demands) {
        this.demands = demands;
    }

    public PaginationMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(PaginationMetaData metaData) {
        this.metaData = metaData;
    }

}
