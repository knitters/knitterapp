package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class DemandUser {

    @SerializedName("id") String id;
    @SerializedName("title") private String title;
    @SerializedName("description") private String description;
    //@SerializedName("author") String authorId;
    @SerializedName("createdAt") String createdAt;
    @SerializedName("updatedAt") String updatedAt;
    @SerializedName("status") String status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
