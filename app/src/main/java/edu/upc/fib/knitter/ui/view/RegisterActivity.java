package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.Constants;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.AuthHashGenerator;
import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import edu.upc.fib.knitter.ui.presenter.RegisterPresenter;

public class RegisterActivity extends BaseActivity implements RegisterPresenter.View {

    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    @Inject
    RegisterPresenter presenter;
    @BindView(R.id.et_phone) EditText etPhone;
    @BindView(R.id.et_year_of_birth) EditText etYearOfBirth;
    @BindView(R.id.b_register) Button bRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();

        mPrefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, RegisterActivity.class);
        context.startActivity(intent);
    }

    @Override
    public Context getContext(){
        return this;
    }


    @Override public int getLayoutId(){return R.layout.activity_register;}

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }


    private void initializePresenter() {
        presenter.setView(this);
        presenter.initialize();
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Check if user is elder
                // TODO Actually backend should return if he is elder on current date
                presenter.registerUser(Integer.parseInt(etPhone.getText().toString()), Integer.parseInt(etYearOfBirth.getText().toString()), true);
            }
        });
    }

    @Override
    public void showMessage(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToMain() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    @Override
    public void loginUser(int phoneNumber) {
        String hash = AuthHashGenerator.generateHashSHA256(phoneNumber + Constants.API_SALT);
        presenter.loginUser(phoneNumber, hash);
    }

    @Override
    public void saveUserData(LoginResponse response) {
        mEditor = mPrefs.edit();
        String token = response.getToken();
        mEditor.putString("token", token);
        mEditor.commit();
    }

}
