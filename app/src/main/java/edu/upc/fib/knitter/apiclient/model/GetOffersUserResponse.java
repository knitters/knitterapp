package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOffersUserResponse {

    @SerializedName("offers") private List<OfferUser> offers;
    @SerializedName("metadata") private PaginationMetaData metaData;

    public List<OfferUser> getOffers() {
        return offers;
    }

    public void setOffers(List<OfferUser> offers) {
        this.offers = offers;
    }

    public PaginationMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(PaginationMetaData metaData) {
        this.metaData = metaData;
    }

}
