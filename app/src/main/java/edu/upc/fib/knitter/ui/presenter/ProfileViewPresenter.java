package edu.upc.fib.knitter.ui.presenter;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.usecase.GetUser;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileViewPresenter extends Presenter<ProfileViewPresenter.View> {

    private User user;
    private final GetUser getUser;

    @Inject
    public ProfileViewPresenter(GetUser getUser) {
        this.getUser = getUser;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void profileView() {
        final ProfileViewPresenter.View view = getView();
        getUser.getUser(this.user.getFirstName(), new GetUser.Callback() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                view.hideLoading();
                view.onSuccess();
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.hideLoading();
                view.onFailure();
            }

        });
    }

    @Override public void initialize() {
        super.initialize();
        View view = getView();
        view.showLoading();
        profileView();
        if (user!= null) {
            view.showUser(user);
        }
    }

    public interface View extends Presenter.View {
        void showUser(User user);
        void onSuccess();
        void onFailure();
    }
}
