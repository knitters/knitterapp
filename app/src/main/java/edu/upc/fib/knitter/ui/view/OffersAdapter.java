package edu.upc.fib.knitter.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.ui.presenter.OffersPresenter;

class OffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final OffersPresenter presenter;
    private final List<Offer> offers;
    private int numberElements;

    public OffersAdapter(OffersPresenter presenter) {
        this.presenter = presenter;
        this.offers = new ArrayList<>();
    }

    void addAll(Collection<Offer> collection, int nElem) {
        offers.clear();
        offers.addAll(collection);
        this.numberElements = nElem;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_row, parent, false);
        return new OffersViewHolder(view, presenter, numberElements);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        OffersViewHolder offersViewHolder = (OffersViewHolder) holder;
        Offer offer = offers.get(position);
        offersViewHolder.render(offer);
    }

    @Override public int getItemCount() {
        return offers.size();
    }
}