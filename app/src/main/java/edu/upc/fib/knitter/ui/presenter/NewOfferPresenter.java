package edu.upc.fib.knitter.ui.presenter;

import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.Category;
import edu.upc.fib.knitter.usecase.GetCategories;
import edu.upc.fib.knitter.usecase.NewOffer;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class NewOfferPresenter extends Presenter<NewOfferPresenter.View> {

    private final NewOffer newOffer;
    private final GetCategories getCategories;

    @Inject
    NewOfferPresenter(NewOffer offer, GetCategories getCategories) {
        this.newOffer = offer;
        this.getCategories = getCategories;
    }

    @Override public void initialize() {
        super.initialize();
    }

    public void postOffer(String description, String title, MultipartBody.Part image, String categoryId) {
        RequestBody description2 =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, description);
        RequestBody title2 =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, title);
        RequestBody categoryId2 =
                RequestBody.create(
                        okhttp3.MultipartBody.FORM, categoryId);

        newOffer.postOffer(description2, title2, image, categoryId2, new NewOffer.Callback() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.isSuccessful()) {
                    getView().showMessage("Oferta añadida satisfactoriamente");
                    getView().goToMenu();
                } else {
                    // error response, no access to resource
                    getView().showMessage("No se ha podido añadir la oferta");
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());
            }
        });
    }

    public void getCategories() {
        getCategories.getCategories(new GetCategories.Callback() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    getView().setCategorySpinner(response.body());
                } else {
                    // error response, no access to resource?
                    getView().goToMenu();
                    getView().showMessage("Can't get categories");
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());
            }
        });
    }

    public interface View extends Presenter.View {
        void setCategorySpinner(List<Category> list);
        void goToMenu();
        void showMessage(String message);
    }
}