package edu.upc.fib.knitter.ui.presenter;

import javax.inject.Inject;

public class PendingPetitionListPresenter extends Presenter<PendingPetitionListPresenter.View>{

    @Inject
    public PendingPetitionListPresenter(){}


    public interface View extends Presenter.View{

    }

}
