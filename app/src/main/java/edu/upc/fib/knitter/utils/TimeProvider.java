package edu.upc.fib.knitter.utils;

public class TimeProvider {
    public long currentTimeMillis() {
        return System.currentTimeMillis();
    }
}
