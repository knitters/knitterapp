package edu.upc.fib.knitter.ui.presenter;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.OfferUser;
import edu.upc.fib.knitter.usecase.GetUserOffers;
import retrofit2.Call;
import retrofit2.Response;

public class UserOffersPresenter extends Presenter<UserOffersPresenter.View> {

    private final GetUserOffers getOffers;
    private List<OfferUser> offers;
    UserOffersPresenter.View view;
    private int page;

    boolean isFirst;
    boolean isLast;

    @Inject
    public UserOffersPresenter(GetUserOffers getOffers) {
        this.getOffers = getOffers;
    }

    @Override public void initialize() {
        super.initialize();
        view = getView();
        view.showLoading();
        page = 0;
        getOffers();
    }

    public void getOffers() {
        getOffers.getUserOffers(new GetUserOffers.Callback() {

            @Override
            public void onResponse(Call<List<OfferUser>> call, Response<List<OfferUser>> response) {
                if (response.isSuccessful()) {
                    offers = response.body();
                    showPage();
                }
            }

            @Override
            public void onFailure(Call<List<OfferUser>> call, Throwable t) {
                view.hideLoading();
                view.showEmptyCase();
                view.hidePrev();
                view.hideNext();
            }
        });
    }

    public void nextPage() {
        page++;
        showPage();
    }

    public void prevPage() {
        page--;
        showPage();
    }

    public void showPage() {

        view.showLoading();

        int pageSize = getView().getPageSize();
        int numElements = pageSize/200;

        if (page == 0) {
            isFirst = true;
            view.hidePrev();
        } else {
            isFirst = false;
            view.showPrev();
        }

        int endBound = (page+1)*numElements;

        if (offers.size() > endBound) {
            isLast = false;
            view.showNext();
        } else {
            isLast = true;
            view.hideNext();
        }

        if (endBound > offers.size()) {
            endBound = offers.size();
        }
        List<OfferUser> subList = offers.subList(page*numElements, endBound);
        view.showOffers(subList, subList.size());
        view.hideLoading();
    }

    public void onOfferClicked(OfferUser offer) {
        getView().openOfferScreen(offer);
    }

    public interface View extends Presenter.View {

        void showEmptyCase();

        void hideEmptyCase();

        void showPrev();

        void hidePrev();

        void showNext();

        void hideNext();

        int getPageSize();

        void showOffers(List<OfferUser> offers, int nElem);

        void openOfferScreen(OfferUser offer);

    }
}