package edu.upc.fib.knitter.usecase;

import android.net.Uri;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class ModifyOffer {

    private final ApiRepository repository;

    @Inject
    public ModifyOffer(ApiRepository repository) {
        this.repository = repository;
    }

    public void doSave(final RequestBody id, final RequestBody title, final RequestBody description,
                       final MultipartBody.Part image, final RequestBody categoryId, final Callback callback){

        new Thread(new Runnable() {
            @Override public void run() {
                Call<MessageResponse> response = repository.updateOffer(id, title, description, image, categoryId);
                //if (imagePath != null) {
                //    response = repository.updateOfferPhoto(id, title, description, imagePath);
                //}
                //else {
                //    response = repository.updateOffer(id, title, description);
                //}
                response.enqueue(new retrofit2.Callback<MessageResponse>() {
                    @Override
                    public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<MessageResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<MessageResponse> call, Response<MessageResponse> response);

        void onFailure(Call<MessageResponse> call, Throwable t);
    }
}
