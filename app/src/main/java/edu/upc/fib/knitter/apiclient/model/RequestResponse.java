package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class RequestResponse {

    @SerializedName("offer") OfferRequest offerRequest;
    @SerializedName("author") String author;
    @SerializedName("id") String id;

    public OfferRequest getOfferRequest() {
        return offerRequest;
    }

    public void setOfferRequest(OfferRequest offerName) {
        this.offerRequest = offerName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    RequestResponse(OfferRequest offerRequest, String author, String id)
    {
        this.offerRequest = offerRequest;
        this.author = author;
        this.id = id;
    }


}
