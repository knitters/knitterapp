package edu.upc.fib.knitter.ui.presenter;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.usecase.GetUserDemands;
import retrofit2.Call;
import retrofit2.Response;

public class UserDemandsPresenter extends Presenter<UserDemandsPresenter.View> {

    private final GetUserDemands getDemands;
    private List<DemandUser> demands;
    UserDemandsPresenter.View view;
    private int page;

    boolean isFirst;
    boolean isLast;

    @Inject
    public UserDemandsPresenter(GetUserDemands getDemands) {
        this.getDemands = getDemands;
    }

    @Override public void initialize() {
        super.initialize();
        view = getView();
        view.showLoading();
        page = 0;
        getDemands();
    }

    public void getDemands() {
        getDemands.getUserDemands(new GetUserDemands.Callback() {

            @Override
            public void onResponse(Call<List<DemandUser>> call, Response<List<DemandUser>> response) {
                if (response.isSuccessful()) {
                    demands = response.body();
                    showPage();
                }
            }

            @Override
            public void onFailure(Call<List<DemandUser>> call, Throwable t) {
                view.hideLoading();
                view.showEmptyCase();
                view.hidePrev();
                view.hideNext();
            }
        });
    }

    public void nextPage() {
        page++;
        showPage();
    }

    public void prevPage() {
        page--;
        showPage();
    }

    public void showPage() {

        view.showLoading();

        int pageSize = getView().getPageSize();
        int numElements = pageSize/150;

        if (page == 0) {
            isFirst = true;
            view.hidePrev();
        } else {
            isFirst = false;
            view.showPrev();
        }

        int endBound = (page+1)*numElements;

        if (demands.size() > endBound) {
            isLast = false;
            view.showNext();
        } else {
            isLast = true;
            view.hideNext();
        }

        if (endBound > demands.size()) {
            endBound = demands.size();
        }
        List<DemandUser> subList = demands.subList(page*numElements, endBound);
        view.showDemands(subList, subList.size());
        view.hideLoading();
    }

    public void onDemandClicked(DemandUser demand) {
        getView().openDemandScreen(demand);
    }

    public interface View extends Presenter.View {

        void showEmptyCase();

        void hideEmptyCase();

        void showPrev();

        void hidePrev();

        void showNext();

        void hideNext();

        int getPageSize();

        void showDemands(List<DemandUser> demands, int nElem);

        void openDemandScreen(DemandUser demand);

    }
}