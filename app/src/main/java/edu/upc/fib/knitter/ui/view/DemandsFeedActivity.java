package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.PaginationMetaData;
import edu.upc.fib.knitter.ui.presenter.DemandsPresenter;

public class DemandsFeedActivity extends BaseActivity implements DemandsPresenter.View {

    @Inject
    DemandsPresenter presenter;

    private DemandsAdapter adapter;
    private PaginationMetaData paginationMetaData;

    @BindView(R.id.tv_empty_case) View emptyCaseView;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    @BindView(R.id.Prev) ImageButton prev;
    @BindView(R.id.Next) ImageButton next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();
        initializeAdapter();
        initializeRecyclerView();
        presenter.initialize();

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!paginationMetaData.isFirst()) {
                    presenter.setPage(paginationMetaData.getPrevCursor(), null);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!paginationMetaData.isLast()) {
                    presenter.setPage(null, paginationMetaData.getNextCursor());
                }
            }
        });
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, DemandsFeedActivity.class);
        context.startActivity(intent);
    }

    @Override
    public int getLayoutId() {
        return R.layout.demands_feed;
    }

    @Override
    public void showDemands(List<Demand> demands, int nElem) {
        adapter.addAll(demands, nElem);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getPageSize(){
        int pxToDp = this.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT;
        return this.getResources().getDisplayMetrics().heightPixels / pxToDp;
    }

    @Override
    public void showPrev(){
        prev.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePrev(){
        prev.setVisibility(View.GONE);
    }

    @Override
    public void showNext(){
        next.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNext(){
        next.setVisibility(View.GONE);
    }

    @Override
    public void openDemandScreen(Demand demand) {
        DemandDetailActivity.open(this, new Gson().toJson(demand));
    }

    @Override
    public void savePaginationMetaData(PaginationMetaData paginationMetaData) {
        this.paginationMetaData = paginationMetaData;
    }

    @Override
    public void showEmptyCase() {
        emptyCaseView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyCase() {
        emptyCaseView.setVisibility(View.GONE);
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
    }

    private void initializeAdapter() {
        adapter = new DemandsAdapter(presenter);
    }

    private void initializeRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }
}