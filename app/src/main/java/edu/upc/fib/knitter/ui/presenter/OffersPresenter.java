package edu.upc.fib.knitter.ui.presenter;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.GetOffersResponse;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.apiclient.model.PaginationMetaData;
import edu.upc.fib.knitter.usecase.GetOffers;
import retrofit2.Call;
import retrofit2.Response;

public class OffersPresenter extends Presenter<OffersPresenter.View> {

    private final GetOffers getOffers;

    @Inject
    public OffersPresenter(GetOffers getOffers) {
        this.getOffers = getOffers;
    }

    @Override public void initialize() {
        setPage(null, null);
    }

    public void setPage(String prevCursor, String nextCursor){
        super.initialize();
        final View view = getView();
        view.showLoading();

        int numElements = getView().getPageSize()/200;

        getOffers.getOffers(numElements, prevCursor, nextCursor, new GetOffers.Callback() {

            @Override
            public void onResponse(Call<GetOffersResponse> call, Response<GetOffersResponse> response) {
                view.hideLoading();
                if (response.isSuccessful() && response.body().getMetaData().getSize() != 0) {
                    if (response.body().getMetaData().isFirst()) {
                        view.hidePrev();
                    } else {
                        view.showPrev();
                    }
                    if (response.body().getMetaData().isLast()) {
                        view.hideNext();
                    } else {
                        view.showNext();
                    }
                    view.savePaginationMetaData(response.body().getMetaData());
                    view.showOffers(response.body().getOffers(), response.body().getMetaData().getSize());
                } else {
                    view.showEmptyCase();
                    view.hidePrev();
                    view.hideNext();
                }
            }

            @Override
            public void onFailure(Call<GetOffersResponse> call, Throwable t) {
                view.hideLoading();
                view.hidePrev();
                view.hideNext();
                view.showEmptyCase();
            }
        });
    }

    public void onOfferClicked(Offer offer) {
        getView().openOfferScreen(offer);
    }

    public interface View extends Presenter.View {

        void showEmptyCase();

        void hideEmptyCase();

        void showPrev();

        void hidePrev();

        void showNext();

        void hideNext();

        int getPageSize();

        void showOffers(List<Offer> offers, int nElem);

        void openOfferScreen(Offer offer);

        void savePaginationMetaData(PaginationMetaData paginationMetaData);
    }
}