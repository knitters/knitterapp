package edu.upc.fib.knitter;

import android.app.Application;
import android.support.annotation.VisibleForTesting;

import edu.upc.fib.knitter.di.DaggerMainComponent;
import edu.upc.fib.knitter.di.MainComponent;
import edu.upc.fib.knitter.di.MainModule;
import edu.upc.fib.knitter.di.NetworkModule;

public class KnitterApplication extends Application {

    private MainComponent mainComponent;

    @Override public void onCreate() {
        super.onCreate();
        mainComponent = DaggerMainComponent.builder().mainModule(new MainModule()).networkModule(new NetworkModule(this)).build();
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }

    @VisibleForTesting
    public void setComponent(MainComponent mainComponent) {
        this.mainComponent = mainComponent;
    }
}
