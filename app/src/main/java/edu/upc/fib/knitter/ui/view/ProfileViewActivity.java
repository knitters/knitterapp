package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.ui.presenter.ProfileViewPresenter;

public class ProfileViewActivity extends BaseActivity implements ProfileViewPresenter.View {

    private static final String USER_OBJECT_KEY = "user_object_key";

    @Inject
    ProfileViewPresenter presenter;

    @BindView(R.id.profileImage) ImageView profileImage;
    @BindView(R.id.nameText) TextView name;
    @BindView(R.id.descText) TextView description;
    @BindView(R.id.retiredText) TextView retired;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();
    }

    @Override public int getLayoutId() {
        return R.layout.profile_view;
    }

    @Override public void showUser(User user) {
        // Show image
        if (!(user.getImagePath()==null)) {
            Picasso.get().load(user.getImagePath()).fit().centerCrop().into(profileImage);
        }

        // Show name
        if (user.getFirstName()!=null && user.getLastName() != null) {
            name.setText(user.getFirstName().concat(" ").concat(user.getLastName()));
        } else if (user.getFirstName() != null) {
            name.setText(user.getFirstName());
        }
        else {
            name.setText(getString(R.string.no_name));
        }

        // Show description
        if (!(user.getDescription()==null)) description.setText(user.getDescription());
        else description.setText(getString(R.string.has_no_desc));

        // Show type user
        String ret;
        if (user.getElder()) ret = getString(R.string.user_type_bidding);
        else ret = getString(R.string.user_type_normal);
        retired.setText(ret);
    }

    @Override
    public void onSuccess() {
        Toast.makeText(this,"Requested Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,"Request Failed", Toast.LENGTH_SHORT).show();
    }

    public static void open(Context context, String user) {
        Intent intent = new Intent(context, ProfileViewActivity.class);
        intent.putExtra(USER_OBJECT_KEY, user);
        context.startActivity(intent);
    }

    @Override protected void initializeToolbar() {
        super.initializeToolbar();
        if (getUser()!=null) {
            setTitle(getUser().getFirstName());
        }
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.setUser(getUser());
        presenter.initialize();
    }

    private User getUser() {
        return new Gson().fromJson(getIntent().getExtras()
                .getString(USER_OBJECT_KEY, ""), User.class);
    }
}
