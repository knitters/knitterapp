package edu.upc.fib.knitter.ui.presenter;

import android.net.Uri;
import android.util.Log;

import java.io.File;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.usecase.ModifyUser;
import edu.upc.fib.knitter.usecase.UserProfile;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class ProfileModifyPresenter extends Presenter<ProfileModifyPresenter.View>{

    private final ModifyUser modifyUser;
    private final UserProfile userProfile;

    @Inject
    ProfileModifyPresenter(ModifyUser modifyUser, UserProfile userProfile) {
        this.modifyUser = modifyUser;
        this.userProfile = userProfile;
    }

    @Override public void initialize() {
        super.initialize();
        getView().hideLoading();
        profileView();
    }

    public void updateUser(String firstName, String lastName, String description, MultipartBody.Part image) {

        RequestBody firstNamePart = RequestBody.create(okhttp3.MultipartBody.FORM, firstName);
        RequestBody lastNamePart = RequestBody.create(okhttp3.MultipartBody.FORM, lastName);
        RequestBody descriptionPart = RequestBody.create(okhttp3.MultipartBody.FORM, description);

        modifyUser.updateUser(firstNamePart, lastNamePart, descriptionPart, image, new ModifyUser.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.isSuccessful()) {
                    getView().showMessage("Usuario actualizado satisfactoriamente");
                    getView().goToMenu();
                } else {
                    // error response, no access to resource
                    getView().showMessage("No se ha podido actualizar el usuario");
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());
            }
        });
    }

    public void profileView() {
        final ProfileModifyPresenter.View view = getView();
        userProfile.getProfile(new UserProfile.Callback() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    view.hideLoading();
                    //view.showMessage("Success");
                    User myUser = response.body();
                    if (myUser!= null) view.setEditText(myUser);
                }
                else { view.showMessage("Failure"); }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.hideLoading();
                view.showMessage("Rrequest Failed");
            }
        });
    }

    public interface View extends Presenter.View{
        void setEditText(User user);
        void goToMenu();
        void showMessage(String message);

    }
}
