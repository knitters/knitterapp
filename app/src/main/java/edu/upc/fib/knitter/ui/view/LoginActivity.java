package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.Constants;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.AuthHashGenerator;
import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import edu.upc.fib.knitter.ui.presenter.LoginPresenter;

public class LoginActivity extends BaseActivity implements LoginPresenter.View {

    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    @Inject
    LoginPresenter presenter;
    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.b_login)
    Button bLogin;
    @BindView(R.id.b_register)
    Button bRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();

        mPrefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    public Context getContext(){
        return this;
    }

    @Override public int getLayoutId(){return R.layout.activity_login;}

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.initialize();
        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO: Check if user is elder
                int phone = Integer.parseInt(etPhone.getText().toString());
                String hash = AuthHashGenerator.generateHashSHA256(phone + Constants.API_SALT);
                presenter.loginUser(phone, hash);
            }
        });
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToRegister();
            }
        });
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveUserToken(LoginResponse response) {
        mEditor = mPrefs.edit();
        mEditor.putString("token", response.getToken());
        mEditor.commit();
    }

    @Override
    public void navigateToMain() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public void navigateToRegister() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

}
