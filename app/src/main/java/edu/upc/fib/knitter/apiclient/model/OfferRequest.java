package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class OfferRequest {

    @SerializedName("title") String title;
    @SerializedName("author") String author;
    @SerializedName("id") String id;
    @SerializedName("status") String status;

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    OfferRequest(String title, String author, String id, String status){
        this.title = title;
        this.author = author;
        this.id = id;
        this.status = status;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
