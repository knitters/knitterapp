package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class MessageResponse {

    @SerializedName("message") String message;

    public MessageResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
