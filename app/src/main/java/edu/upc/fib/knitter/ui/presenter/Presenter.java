package edu.upc.fib.knitter.ui.presenter;

import edu.upc.fib.knitter.ui.view.BaseActivity;
import edu.upc.fib.knitter.ui.view.OfferDetailActivity;

public class Presenter<T extends Presenter.View> {

    private T view;

    public void setView(T view) {
        this.view = view;
    }

    public T getView() {
        return view;
    }

    public void initialize() {}

    public void update() {}

    public interface View {

        void showLoading();

        void hideLoading();

    }
}