package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import retrofit2.Call;
import retrofit2.Response;

public class AcceptOffer {

    private final ApiRepository apiRepository;

    @Inject
    public AcceptOffer(ApiRepository apiRepository){
        this.apiRepository = apiRepository;
    }

    public interface Callback {
        void onResponse(Call<MessageResponse> call, Response<MessageResponse> response);
        void onFailure(Call<MessageResponse> call, Throwable t);
    }


    public void doAcceptOffer(final String id, final AcceptOffer.Callback callback){
        new Thread(new Runnable() {
            @Override public void run() {
                Call<MessageResponse> response = apiRepository.acceptOffer(id);
                response.enqueue(new retrofit2.Callback<MessageResponse>() {
                    @Override
                    public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<MessageResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }
}
