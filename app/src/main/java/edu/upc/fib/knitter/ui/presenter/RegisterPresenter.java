package edu.upc.fib.knitter.ui.presenter;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.usecase.Login;
import edu.upc.fib.knitter.usecase.Register;
import retrofit2.Call;
import retrofit2.Response;

public class RegisterPresenter extends Presenter<RegisterPresenter.View> {

    private final Register register;
    private final Login login;

    @Inject
    public RegisterPresenter(Register register, Login login) {
        this.register = register;
        this.login = login;
    }

    @Override public void initialize() {
        super.initialize();
        View view = getView();
        view.hideLoading();
    }

    public void registerUser(final int phoneNumber, int yearOfBirth, boolean isElder) {
        register.registerUser(phoneNumber, yearOfBirth, isElder, new Register.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.isSuccessful()) {
                    getView().showMessage("Usuario registrado correctamente");
                    getView().loginUser(phoneNumber);
                } else {
                    getView().showMessage("No se ha podido registrar el usuario");
                    // error response, no access to resource?
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());
            }
        });
    }

    public void loginUser(int phoneNumber, String hash) {
        login.loginUser(phoneNumber, hash, new Login.Callback() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    getView().saveUserData(response.body());
                    getView().navigateToMain();
                } else {
                    // error response, no access to resource?
                    getView().showMessage("No se ha podido hacer login");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());
            }
        });
    }


    public interface View extends Presenter.View{
        Context getContext();
        void showMessage(String message);
        void navigateToMain();
        void loginUser(int phoneNumber);
        void saveUserData(LoginResponse response);

    }
}
