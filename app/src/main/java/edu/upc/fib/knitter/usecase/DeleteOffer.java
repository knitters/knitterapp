package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.GetOffersResponse;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.Offer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DeleteOffer {

    private final ApiRepository repository;

    @Inject
    public DeleteOffer(ApiRepository repository) {
        this.repository = repository;
    }

    public interface Callback {
        void onResponse(Call<ResponseBody> call, Response<ResponseBody> response);
        void onFailure(Call<ResponseBody> call, Throwable t);
    }

    public void doDelete(final String id, final Callback callback){
        new Thread(new Runnable() {
            @Override public void run() {
                Call<ResponseBody> response = repository.deleteOffer(id);
                response.enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }
}
