package edu.upc.fib.knitter.ui.presenter;

import android.net.Uri;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.usecase.DeletePetition;
import edu.upc.fib.knitter.usecase.ModifyDemand;
import edu.upc.fib.knitter.usecase.ModifyOffer;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DemandModifyPresenter extends Presenter<DemandModifyPresenter.View>{

    private DemandUser demand;
    private DeletePetition deletePetition;
    private ModifyDemand modifyDemand;

    @Inject
    public DemandModifyPresenter(DeletePetition deletePetition, ModifyDemand modifyDemand) {
        this.deletePetition = deletePetition;
        this.modifyDemand = modifyDemand;
    }


    public void setDemand(DemandUser demand) {
        this.demand = demand;
    }

    public void doSave(String description){
        final DemandModifyPresenter.View view = getView();
        view.showLoading();
        modifyDemand.doSave(this.demand.getId(), demand.getTitle(), description, new ModifyDemand.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                view.hideLoading();
                view.onSaveSuccess();
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                view.hideLoading();
                view.onSaveFailure();
            }
        });
    }

    public void doDelete() {
        final DemandModifyPresenter.View view = getView();
        view.showLoading();
        deletePetition.doDelete(demand.getId(), new DeletePetition.Callback() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                view.hideLoading();
                view.onDeleteSuccess();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideLoading();
                view.onDeleteFailure();
            }
        });
    }

    @Override public void initialize() {
        super.initialize();
        View view = getView();
        view.hideLoading();
        view.showDemand(demand);
    }

    public interface View extends Presenter.View{
        void showDemand(DemandUser demand);
        void onSaveFailure();
        void onSaveSuccess();
        void onDeleteFailure();
        void onDeleteSuccess();
    }
}
