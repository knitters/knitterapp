package edu.upc.fib.knitter.Chat;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.Chat.Model.ChatMessage;
import edu.upc.fib.knitter.Chat.Model.ChatUser;
import edu.upc.fib.knitter.Chat.Model.ChatUserAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatsFragment extends Fragment {

    private RecyclerView recyclerView;

    private ChatUserAdapter userAdapter;
    private List<ChatUser> lUser;
    DatabaseReference reference;
    FirebaseUser fuser;

    private List<String> usersList;


    public ChatsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chats, container, false);

        recyclerView = view.findViewById(R.id.recycler_list_chats);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        fuser = FirebaseAuth.getInstance().getCurrentUser();

        usersList = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear();

                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    ChatMessage message = snapshot.getValue(ChatMessage.class);

                    if(message.getSender().equals(fuser.getUid())){
                        usersList.add(message.getReceiver());
                    }
                    if(message.getReceiver().equals(fuser.getUid())){
                        usersList.add(message.getSender());
                    }
                }
                readChats();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return view;


    }

    private void readChats(){
        lUser = new ArrayList<>();

        reference = FirebaseDatabase.getInstance().getReference("Users");

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lUser.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()){

                    String name, image, id, phone;
                    id = snapshot.getKey();
                    phone = snapshot.child("phone").getValue(String.class);
                    name = snapshot.child("name").getValue(String.class);
                    image = snapshot.child("image").getValue(String.class);
                    ChatUser user = new ChatUser(fuser.getUid(), phone, name, image);
                    for(String idList : usersList){
                        if(user.getId().equals(idList)){
                            if(lUser.size() != 0){
                                for(ChatUser userL : lUser){
                                    if(!user.getId().equals(userL.getId())){
                                        lUser.add(user);
                                    }
                                }
                            }
                            else {
                                lUser.add(user);
                            }
                        }
                    }
                }
                userAdapter = new ChatUserAdapter(getContext(), lUser);
                recyclerView.setAdapter(userAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
