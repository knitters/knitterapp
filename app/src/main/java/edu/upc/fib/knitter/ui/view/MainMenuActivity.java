package edu.upc.fib.knitter.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import edu.upc.fib.knitter.Chat.ChatLoginActivity;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.ui.presenter.MainMenuPresenter;

public class MainMenuActivity extends BaseActivity implements MainMenuPresenter.View {

    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;
    User user;

    @Inject
    MainMenuPresenter presenter;

    @BindView(R.id.ll_explore_demands) LinearLayout searchDemands;
    @BindView(R.id.ll_explore_offers) LinearLayout searchOffers;
    @BindView(R.id.ll_my_demands) LinearLayout searchMyDemand;
    @BindView(R.id.ll_my_offers) LinearLayout searchMyOffers;
    @BindView(R.id.ll_publish_demand) LinearLayout publishDemand;
    @BindView(R.id.ll_publish_offer) LinearLayout publishOffer;
    @BindView(R.id.rl_user) RelativeLayout userButton;
    @BindView(R.id.chatButton) Button chatButton;
    //@BindView(R.id.pendingButton) Button pendingButton;
    @BindView(R.id.iv_user_image) ImageView userImage;
    @BindView(R.id.tv_user_name) TextView userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPrefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();

        initializeDagger();
        initializePresenter();

        showLoading();

        searchOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onOfferSearchClick();
            }
        });
        searchDemands.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onDemandSearchClick();
            }
        });
        searchMyOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onMyOfferSearchClick();
            }
        });
        searchMyDemand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onMyDemandSearchClick();
            }
        });
        publishOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onOfferPublishClick();
            }
        });
        publishDemand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onDemandPublishClick();
            }
        });
        userButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onUserClick();
            }
        });
        hideLoading();

        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onChatClick();
                }
        });

        /*
        pendingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onPendingClick();
            }
        });
        */
    }

    private void initializeView() {
        hideLoading();

        String user = mPrefs.getString("firstName", "Anonimous") + " " + mPrefs.getString("lastName", "");
        userName.setText(user);

        if (mPrefs.getBoolean("isElder", true)){
            userImage.setImageResource(R.drawable.ic_older_adult);
            searchMyDemand.setVisibility(View.GONE);
            publishDemand.setVisibility(View.GONE);
            searchOffers.setVisibility(View.GONE);
        }
        else {
            userImage.setImageResource(R.drawable.ic_adult);
            searchMyOffers.setVisibility(View.GONE);
            publishOffer.setVisibility(View.GONE);
            searchDemands.setVisibility(View.GONE);
        }

        String photo = mPrefs.getString("imagePath", "");
        if (!photo.isEmpty()) Picasso.get().load(photo).fit().centerInside().into(userImage);

    }

    @Override public void setUser (User user) {
        this.user = user;
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.initialize();
        presenter.setView(this);
    }


    @OnClick(R.id.b_logout)
    public void onLogoutClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialog);
        builder.setTitle("Logout");
        builder.setMessage("Seguro que desea cerrar sesión?");
        builder.setPositiveButton("Cerrar sesion", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                logout();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                builder.dismiss();
            }
        });

        builder.show();
    }

    public void logout() {
        mEditor.clear();
        mEditor.commit();
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
    }

    @Override
    public int getLayoutId() {
        return R.layout.main_menu;
    }

    @Override
    public void openOfferSearch() {
        OffersFeedActivity.open(this);
    }

    @Override
    public void openDemandSearch() {
        DemandsFeedActivity.open(this);
    }

    @Override
    public void openMyDemandSearch() {
        UserDemandsActivity.open(this, false);
    }

    @Override
    public void openMyOfferSearch() {
        UserOffersActivity.open(this);
    }

    @Override
    public void openOfferPublish(){
        NewOfferActivity.open(this);
    }

    @Override
    public void openDemandPublish(){
        NewDemandActivity.open(this);
    }

    @Override
        public void openChat(){ ChatLoginActivity.open(this);}

    @Override
    public void openPendingDemands(){ PendingDemandListActivity.open(this);}

    @Override
    public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveUserInfo(User response) {
        mEditor.putString("id", response.getId());
        mEditor.putInt("phoneNumber", response.getPhoneNumber());
        mEditor.putString("firstName", response.getFirstName());
        mEditor.putString("lastName", response.getLastName());
        mEditor.putString("imagePath", response.getImagePath());
        mEditor.putString("description", response.getDescription());
        mEditor.putBoolean("isElder", response.getElder());
        mEditor.commit();

        initializeView();
    }

    @Override
    public void openUser() {
        if (user.getFirstName()==null)  ProfileModifyActivity.open(this);
        else MyProfileViewActivity.open(this);
    }

}
