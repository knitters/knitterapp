package edu.upc.fib.knitter.ui.presenter;

import android.content.Context;
import android.util.Log;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import edu.upc.fib.knitter.usecase.Login;
import retrofit2.Call;
import retrofit2.Response;

public class LoginPresenter extends Presenter<LoginPresenter.View> {

    private final Login login;

    @Inject
    public LoginPresenter(Login login) {
        this.login = login;
    }

    @Override public void initialize() {
        super.initialize();
        LoginPresenter.View view = getView();
        view.hideLoading();
    }

    public void loginUser(int phoneNumber, String hash) {
        login.loginUser(phoneNumber, hash, new Login.Callback() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {
                    getView().saveUserToken(response.body());
                    getView().navigateToMain();
                } else {
                    // error response, no access to resource?
                    getView().showMessage("No se ha podido hacer login");
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessage(t.getMessage());
            }
        });
    }

    public interface View extends Presenter.View{
        Context getContext();
        void showMessage(String message);
        void saveUserToken(LoginResponse response);
        void navigateToMain();
    }
}
