package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetOffersResponse {

    @SerializedName("offers") private List<Offer> offers;
    @SerializedName("metadata") private PaginationMetaData metaData;

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public PaginationMetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(PaginationMetaData metaData) {
        this.metaData = metaData;
    }

}
