package edu.upc.fib.knitter.Chat;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.Chat.Model.ChatUser;
import edu.upc.fib.knitter.Chat.Model.ChatUserAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestsFragment extends Fragment {


    private View mainView;
    private RecyclerView requestsList;
    private DatabaseReference databaseReference;
    private FirebaseAuth auth;
    private ChatUserAdapter chatUserAdapter;
    private List<ChatUser> lUsers;

    private String currentUser;

    public RequestsFragment() {
        // Required empty public constructor
    }

    //TODO put number of pending requests between users in description

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_requests, container, false);
        requestsList = view.findViewById(R.id.requests_list);
        requestsList.setHasFixedSize(true);
        requestsList.setLayoutManager(new LinearLayoutManager(getContext()));
        lUsers = new ArrayList<>();
        readRequests();
        return view;
    }

    private void readRequests() {
        final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");

        //TODO call api and filter requests
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lUsers.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //ChatUser user = dataSnapshot.getValue(ChatUser.class);
                    String name, image, id, phone;
                    id = snapshot.getKey();
                    phone = snapshot.child("phone").getValue(String.class);
                    name = snapshot.child("name").getValue(String.class);
                    image = snapshot.child("image").getValue(String.class);
                    ChatUser user = new ChatUser(id,phone,name,image);
                    assert user != null;
                    assert id != null;
                    assert firebaseUser != null;
                    //TODO compare current user id and has requests
                    if(!user.getId().equals(firebaseUser.getUid())){
                        lUsers.add(user);
                    }
                }
                chatUserAdapter = new ChatUserAdapter(getContext(), lUsers);
                requestsList.setAdapter(chatUserAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
