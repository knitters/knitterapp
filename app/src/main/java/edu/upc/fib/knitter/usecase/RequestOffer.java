package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import retrofit2.Call;
import retrofit2.Response;

public class RequestOffer {

    private final ApiRepository repository;

    @Inject
    public RequestOffer(ApiRepository repository) {
        this.repository = repository;
    }

    public interface Callback {
        void onResponse(Call<MessageResponse> call, Response<MessageResponse> response);
        void onFailure(Call<MessageResponse> call, Throwable t);
    }

    public void requestDemand(final String id, final Callback callback){
        new Thread(new Runnable() {
            @Override public void run() {
                Call<MessageResponse> response = repository.requestOffer(id);
                response.enqueue(new retrofit2.Callback<MessageResponse>() {
                    @Override
                    public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<MessageResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }
}
