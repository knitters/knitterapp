package edu.upc.fib.knitter.ui.presenter;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.usecase.RequestOffer;
import retrofit2.Call;
import retrofit2.Response;

public class OfferDetailPresenter extends Presenter<OfferDetailPresenter.View> {

    private Offer offer;
    private RequestOffer requestOffer;

    @Inject
    public OfferDetailPresenter(RequestOffer requestOffer) {
        this.requestOffer = requestOffer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public void requestOffer(){
        final OfferDetailPresenter.View view = getView();
        view.showLoading();
        requestOffer.requestDemand(this.offer.getId(), new RequestOffer.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                view.hideLoading();
                view.onRequestSuccess();
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                view.hideLoading();
                view.onRequestFailure();
            }
        });
    }

    @Override public void initialize() {
        super.initialize();
        View view = getView();
        view.hideLoading();
        view.showOffer(offer);
    }

    public interface View extends Presenter.View {
        void showOffer(Offer offer);
        void onRequestSuccess();
        void onRequestFailure();
    }
}
