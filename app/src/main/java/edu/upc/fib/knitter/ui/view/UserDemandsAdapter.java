package edu.upc.fib.knitter.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.ui.presenter.DemandsPresenter;
import edu.upc.fib.knitter.ui.presenter.UserDemandsPresenter;

class UserDemandsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final UserDemandsPresenter presenter;
    private final List<DemandUser> demands;
    private int numberElements;
    private String userName;

    public UserDemandsAdapter(UserDemandsPresenter presenter, String userName) {
        this.presenter = presenter;
        this.demands = new ArrayList<>();
        this.userName = userName;
    }

    void addAll(List<DemandUser> collection, int nElem) {
        demands.clear();
        demands.addAll(collection);
        this.numberElements = nElem;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view =
                LayoutInflater.from(parent.getContext()).inflate(R.layout.demand_row, parent, false);
        return new UserDemandsViewHolder(view, presenter, numberElements);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        UserDemandsViewHolder demandsViewHolder = (UserDemandsViewHolder) holder;
        DemandUser demand = demands.get(position);
        demandsViewHolder.render(demand, userName);
    }

    @Override public int getItemCount() {
        return demands.size();
    }
}