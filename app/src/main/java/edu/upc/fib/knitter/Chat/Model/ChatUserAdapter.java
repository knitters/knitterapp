package edu.upc.fib.knitter.Chat.Model;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.ui.view.ChatActivity;


public class ChatUserAdapter extends RecyclerView.Adapter<ChatUserAdapter.myViewHolder> {

    private Context context;
    private List<ChatUser> lUser;

    public ChatUserAdapter(Context context, List<ChatUser> luser){
        this.context = context;
        this.lUser = luser;
    }


    @NonNull
    @Override
    public myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.chat_user_single, viewGroup, false);
        return new myViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull myViewHolder viewHolder, int i) {
        final ChatUser user = lUser.get(i);
        viewHolder.username.setText(user.getName());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ChatActivity.class);
                intent.putExtra("chat_user_id", user.getId());
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return lUser.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {

        public TextView username;
        public ImageView profileImage;

        public myViewHolder(View view){
            super(view);

            username = view.findViewById(R.id.tv_displayname);
        }

    }
}
