package edu.upc.fib.knitter.apiclient;

import android.net.Uri;

import java.util.List;

import edu.upc.fib.knitter.apiclient.model.Category;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.apiclient.model.GetDemandsResponse;
import edu.upc.fib.knitter.apiclient.model.GetOffersResponse;
import edu.upc.fib.knitter.apiclient.model.GetDemandsUserResponse;
import edu.upc.fib.knitter.apiclient.model.GetOffersUserResponse;
import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.OfferUser;
import edu.upc.fib.knitter.apiclient.model.RequestResponse;
import edu.upc.fib.knitter.apiclient.model.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ApiService {

    /* DEMAND */

    @GET("demand")
    Call<GetDemandsResponse> getDemands(
            @Query("size") Integer size,
            @Query("prev_cursor") String prevCursor,
            @Query("next_cursor") String nextCursor
    );

    @FormUrlEncoded
    @POST("demand/requestDemand")
    Call<MessageResponse> requestDemand(
            @Field("demandId") String id
    );

    @FormUrlEncoded
    @POST("demand")
    Call<MessageResponse> createDemand(
            @Field("description") String description,
            @Field("title") String title,
            @Field("categoryId") String categoryId
    );

    @FormUrlEncoded
    @PUT("demand")
    Call<MessageResponse> updateDemand(
            @Field("id") String id,
            @Field("title") String title,
            @Field("description") String description
    );
    /*
    @FormUrlEncoded
    @DELETE("demand")
    Call<MessageResponse> deleteDemand(
            @Field("id") String id
    );*/

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "demand", hasBody = true)
    Call<ResponseBody> deleteDemand(@Field("id") String id);

    /* OFFER */

    @GET("offer")
    Call<GetOffersResponse> getOffers(
            @Query("size") Integer size,
            @Query("prev_cursor") String prevCursor,
            @Query("next_cursor") String nextCursor
    );

    @FormUrlEncoded
    @POST("offer/requestOffer")
    Call<MessageResponse> requestOffer(
            @Field("offerId") String id
    );

    @Multipart
    @POST("offer")
    Call<Void> createOffer(
            @Part("description") RequestBody description,
            @Part("title") RequestBody title,
            @Part MultipartBody.Part image,
            @Part("categoryId") RequestBody  categoryId
    );

    @Multipart
    @PUT("offer")
    Call<MessageResponse> updateOffer(
            @Part("id") RequestBody id,
            @Part("title") RequestBody title,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part image,
            @Part("categoryId") RequestBody  categoryId
    );
    /*
    @FormUrlEncoded
    @DELETE("offer/{Id}")
    Call<ResponseBody> deleteOffer(
            @Field("Id") String Id
    );*/
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "offer", hasBody = true)
    Call<ResponseBody> deleteOffer(@Field("id") String id);

    /* USER */

    @FormUrlEncoded
    @POST("users/register")
    Call<MessageResponse> registerUser(
            @Field("phoneNumber") Integer phone,
            @Field("yearOfBirth") Integer yearOfBirth,
            @Field("isElder") Boolean isElder
    );

    @GET("users")
    Call<User> getUsers(
            @Field("filter") String filter
    );

    @FormUrlEncoded
    @POST("users/login")
    Call<LoginResponse> loginUser(
            @Field("phoneNumber") Integer phone,
            @Field("hash") String hash
    );

    @GET("users/profile")
    Call<User> getUserProfile();

    @GET("users/profile/offers")
    Call<List<OfferUser>> getUserOffers();

    @GET("users/profile/demands")
    Call<List<DemandUser>> getUserDemands();

    @Multipart
    @PUT("users/profile")
    Call<MessageResponse> updateUser(
            @Part("firstName") RequestBody firstName,
            @Part("lastName") RequestBody lastName,
            @Part("description") RequestBody description,
            @Part MultipartBody.Part image
    );

    @GET("categories")
    Call<List<Category>> getCategories();

    @GET("xat/getRequests") Call<List<RequestResponse>> getChatRequests();

    @FormUrlEncoded
    @POST("offer/acceptRequest")
    Call<MessageResponse> acceptOffer(
            @Field("requestId") String requestId
    );

    @FormUrlEncoded
    @POST("demand/acceptRequest")
    Call<MessageResponse> acceptDemand(
            @Field("requestId") String requestId
    );

}
