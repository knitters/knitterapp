package edu.upc.fib.knitter;

public class Constants {
    public final static String API_BASE_URL = "https://knitter-pes.herokuapp.com/api/";
    public final static String API_SALT = "LwigmrmPIV";
}
