package edu.upc.fib.knitter.ui.presenter;

import android.net.Uri;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.usecase.DeleteOffer;
import edu.upc.fib.knitter.usecase.ModifyOffer;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class OfferModifyPresenter extends Presenter<OfferModifyPresenter.View> {

    private Offer offer;
    private DeleteOffer deleteOffer;
    private ModifyOffer modifyOffer;

    @Inject
    public OfferModifyPresenter(DeleteOffer deleteOffer, ModifyOffer modifyOffer) {
        this.deleteOffer = deleteOffer;
        this.modifyOffer = modifyOffer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public void doSave(String title, String description, MultipartBody.Part image){
        RequestBody idPart = RequestBody.create(okhttp3.MultipartBody.FORM, this.offer.getId());
        RequestBody titlePart = RequestBody.create(okhttp3.MultipartBody.FORM, title);
        RequestBody descriptionPart = RequestBody.create(okhttp3.MultipartBody.FORM, description);

        final OfferModifyPresenter.View view = getView();
        view.showLoading();
        modifyOffer.doSave(idPart, titlePart, descriptionPart, image, null, new ModifyOffer.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                view.hideLoading();
                view.onSaveSuccess();
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                view.hideLoading();
                view.onSaveFailure();
            }
        });
    }

    public void doDelete() {
        final OfferModifyPresenter.View view = getView();
        view.showLoading();
        deleteOffer.doDelete(offer.getId(), new DeleteOffer.Callback() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                view.hideLoading();
                view.onDeleteSuccess();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideLoading();
                view.onDeleteFailure();
            }
        });
    }

    @Override public void initialize() {
        super.initialize();
        View view = getView();
        view.hideLoading();
        view.showOffer(offer);
    }

    public interface View extends Presenter.View {
        void showOffer(Offer offer);
        void onSaveSuccess();
        void onSaveFailure();
        void onDeleteFailure();
        void onDeleteSuccess();

    }
}
