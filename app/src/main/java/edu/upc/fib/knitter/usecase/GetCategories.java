package edu.upc.fib.knitter.usecase;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.Category;
import retrofit2.Call;
import retrofit2.Response;

public class GetCategories {

    private final ApiRepository repository;

    @Inject
    public GetCategories(ApiRepository repository) {
        this.repository = repository;
    }

    public void getCategories(final Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<List<Category>> response = repository.getCategories();
                response.enqueue(new retrofit2.Callback<List<Category>>() {
                    @Override
                    public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<List<Category>> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<List<Category>> call, Response<List<Category>> response);
        void onFailure(Call<List<Category>> call, Throwable t);
    }
    
}
