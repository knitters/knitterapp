package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.ui.presenter.PendingPetitionListPresenter;

public class PendingDemandListActivity extends BaseActivity implements PendingPetitionListPresenter.View {

    @Inject PendingPetitionListPresenter presenter;

    @BindView(R.id.slv_pending_petition)
    SwipeMenuListView swipeMenuListView;
    ArrayList<String> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePresenter();
        initializeDagger();
        list = new ArrayList<>();
        list.add("Petition 1");
        list.add("Petition 2");
        list.add("Petition 3");
        list.add("Petition 4");
        list.add("Petition 5");
        list.add("Petition 6");
        list.add("Petition 7");
        list.add("Petition 8");
        list.add("Petition 9");
        list.add("Petition 10");

        ArrayAdapter adapter = new ArrayAdapter(PendingDemandListActivity.this, android.R.layout.simple_list_item_1, list);
        swipeMenuListView.setAdapter(adapter);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.GREEN));
                // set item width
                openItem.setWidth(240);
                // set item title
                openItem.setTitle("ACCEPT");
                // set item title fontsize
                openItem.setTitleSize(16);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(240);
                // set a icon
                deleteItem.setIcon(R.drawable.ic_cancel);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        swipeMenuListView.setMenuCreator(creator);

        swipeMenuListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch(index){
                    case 0:
                        //TODO toast case 0
                        Toast.makeText(getApplicationContext(), "ACCEPT CLICKED", Toast.LENGTH_LONG).show();
                        break;
                    case 1:
                        //TODO toast case 1
                        Toast.makeText(getApplicationContext(), "DECLINE CLICKED", Toast.LENGTH_LONG).show();
                        break;
                }
                return false;
            }
        });
    }


    private void initializePresenter(){
        presenter = new PendingPetitionListPresenter();
        presenter.setView(this);
    }


    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    @Override
    public int getLayoutId(){return R.layout.activity_pending_petition_list;}

    public static void open(Context context) {
        Intent intent = new Intent(context, PendingDemandListActivity.class);
        context.startActivity(intent);
    }


}
