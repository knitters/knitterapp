package edu.upc.fib.knitter.ui.presenter;
import com.google.gson.Gson;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.usecase.UserProfile;
import retrofit2.Call;
import retrofit2.Response;

public class MyProfileViewPresenter extends Presenter<MyProfileViewPresenter.View> {

    private User user;
    private final UserProfile userProfile;
    private String json;

    @Inject public MyProfileViewPresenter(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public void setUser(User user) {
        this.user = user;
        getView().showUser(user);
    }

    public void profileView() {
        final MyProfileViewPresenter.View view = getView();
        userProfile.getProfile(new UserProfile.Callback() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    view.hideLoading();
                    // view.onSuccess();
                    user = response.body();
                    setUser(user);
                }
                else { view.onFailure(); }
            }
            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.hideLoading();
                view.onFailure();
            }
        });
    }

    @Override public void initialize() {
        super.initialize();
        profileView();
    }

    public interface View extends Presenter.View {
        void message(String str);
        void showUser(User user);
        void onSuccess();
        void onFailure();
    }
}
