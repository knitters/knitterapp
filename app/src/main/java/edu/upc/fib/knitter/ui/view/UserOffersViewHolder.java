package edu.upc.fib.knitter.ui.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.OfferUser;
import edu.upc.fib.knitter.ui.presenter.UserOffersPresenter;

public class UserOffersViewHolder extends RecyclerView.ViewHolder {

    private final UserOffersPresenter presenter;
    @BindView(R.id.iv_offer_photo) ImageView photoImageView;
    @BindView(R.id.tv_offer_name) TextView nameTextView;
    @BindView(R.id.tv_offer_author) TextView authorTextView;
    @BindView(R.id.info) RelativeLayout layout;
    private int hei, wid;

    public UserOffersViewHolder(View itemView, UserOffersPresenter presenter, int numberElements) {
        super(itemView);
        hei = (itemView.getResources().getDisplayMetrics().heightPixels - 280) / numberElements;
        wid = itemView.getResources().getDisplayMetrics().widthPixels;
        this.presenter = presenter;
        ButterKnife.bind(this, itemView);
    }

    public void render(OfferUser offer, String userName) {
        hookListeners(offer);
        layout.setLayoutParams(new RelativeLayout.LayoutParams(wid,hei));
        renderOfferPhoto(offer.getImagePath());
        renderOfferName(offer.getTitle());
        //renderOfferAuthor(offer.getAuthor().getFirstName());
        renderOfferAuthor(userName);

    }

    private void hookListeners(final OfferUser offer) {
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                presenter.onOfferClicked(offer);
            }
        });
    }

    private void renderOfferPhoto(String photo) {
        if (!photo.isEmpty()) {
            Picasso.get().load(photo).fit().centerCrop().into(photoImageView);
        }
    }

    private void renderOfferName(String name) {
        nameTextView.setText(name);
    }

    private void renderOfferAuthor(String author) {
        authorTextView.setText(author);
    }
}