package edu.upc.fib.knitter.usecase;

import edu.upc.fib.knitter.apiclient.model.Offer;

public class AddCategoryOffer {

    private Offer offer;
    private String categoria;

    public AddCategoryOffer(Offer offer, String categoria){
        this.offer = offer;
        this.categoria = categoria;
    }

    public void doChange(){
        //TODO Call API and link categoria to offer
    }

}
