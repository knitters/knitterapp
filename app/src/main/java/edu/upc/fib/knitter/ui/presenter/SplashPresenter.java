package edu.upc.fib.knitter.ui.presenter;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.GetDemandsResponse;
import edu.upc.fib.knitter.usecase.GetDemands;
import retrofit2.Call;
import retrofit2.Response;

public class SplashPresenter extends Presenter<SplashPresenter.View> {

    private final GetDemands checkServerStatus;

    @Inject
    public SplashPresenter(GetDemands checkServerStatus) {
        this.checkServerStatus = checkServerStatus;
    }

    @Override public void initialize() {
        super.initialize();
        checkServerStatus();
    }

    public void checkServerStatus() {
        checkServerStatus.getDemands(1, null, null, new GetDemands.Callback() {

            @Override
            public void onResponse(Call<GetDemandsResponse> call, Response<GetDemandsResponse> response) {
                if (response.isSuccessful()) {
                    getView().serverOk();
                } else {
                    getView().serverKo();
                }
            }

            @Override
            public void onFailure(Call<GetDemandsResponse> call, Throwable t) {
                getView().serverKo();
            }
        });
    }

    public interface View extends Presenter.View {
        void serverOk();
        void serverKo();
    }
}
