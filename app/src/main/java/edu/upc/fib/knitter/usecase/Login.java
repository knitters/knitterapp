package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import retrofit2.Call;
import retrofit2.Response;

public class Login {

    private final ApiRepository repository;

    @Inject
    public Login(ApiRepository repository){
        this.repository = repository;
    }

    public void loginUser(final int phone, final String hash, final Login.Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {

                Call<LoginResponse> response = repository.loginUser(phone, hash);
                response.enqueue(new retrofit2.Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<LoginResponse> call, Response<LoginResponse> response);
        void onFailure(Call<LoginResponse> call, Throwable t);
    }
}
