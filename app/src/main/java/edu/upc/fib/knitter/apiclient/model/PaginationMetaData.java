package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class PaginationMetaData {

    @SerializedName("size") private int size;
    @SerializedName("prev_cursor") private String prevCursor;
    @SerializedName("next_cursor") private String nextCursor;
    @SerializedName("isFirst") private boolean isFirst;
    @SerializedName("isLast") private boolean isLast;

    public PaginationMetaData(int size, String prevCursor, String nextCursor, boolean isFirst, boolean isLast) {
        this.size = size;
        this.prevCursor = prevCursor;
        this.nextCursor = nextCursor;
        this.isFirst = isFirst;
        this.isLast = isLast;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getPrevCursor() {
        return prevCursor;
    }

    public void setPrevCursor(String prevCursor) {
        this.prevCursor = prevCursor;
    }

    public String getNextCursor() {
        return nextCursor;
    }

    public void setNextCursor(String nextCursor) {
        this.nextCursor = nextCursor;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public boolean isLast() {
        return isLast;
    }

    public void setLast(boolean last) {
        isLast = last;
    }

}
