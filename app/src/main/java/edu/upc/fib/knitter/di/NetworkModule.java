package edu.upc.fib.knitter.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import edu.upc.fib.knitter.Constants;
import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.ApiService;
import edu.upc.fib.knitter.apiclient.AuthenticationInterceptor;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    private Context context;

    public NetworkModule(Application app) {
        this.context = app;
    }

    @Provides
    @Named("network.context")
    Context providesContext() {
        return context;
    }

    @Provides
    @Named("network.base_url")
    String providebaseURL() {
        return Constants.API_BASE_URL;
    }

    @Provides
    OkHttpClient provideOkhttpClient(@Named("network.context")final Context context) {
        final String token = context.getSharedPreferences("Prefs", Context.MODE_PRIVATE).getString("token", "");
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        client.addInterceptor(provideAuthenticationInterceptor(token));
        return client.build();
    }

    @Provides
    Retrofit provideRetrofit(@Named("network.base_url") String baseURL, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(baseURL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    AuthenticationInterceptor provideAuthenticationInterceptor(String authToken){
        return new AuthenticationInterceptor(authToken);
    }

    @Provides
    ApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides public ApiRepository provideApiRepository(ApiService apiService) {
        return new ApiRepository(apiService);
    }

}
