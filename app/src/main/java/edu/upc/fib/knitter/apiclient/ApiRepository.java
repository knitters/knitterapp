package edu.upc.fib.knitter.apiclient;

import android.net.Uri;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.Category;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.apiclient.model.GetDemandsResponse;
import edu.upc.fib.knitter.apiclient.model.GetOffersResponse;
import edu.upc.fib.knitter.apiclient.model.GetDemandsUserResponse;
import edu.upc.fib.knitter.apiclient.model.GetOffersUserResponse;
import edu.upc.fib.knitter.apiclient.model.LoginResponse;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.OfferUser;
import edu.upc.fib.knitter.apiclient.model.RequestResponse;
import edu.upc.fib.knitter.apiclient.model.User;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class ApiRepository implements ApiService {

    public ApiService apiService;

    @Inject
    public ApiRepository(ApiService apiService) {
        this.apiService = apiService;
    }

    /* DEMAND */

    @Override
    public Call<GetDemandsResponse> getDemands(Integer size, String nextCursor, String prevCursor) {
        return apiService.getDemands(size, nextCursor, prevCursor);
    }

    @Override
    public Call<MessageResponse> requestDemand(String id) {
        return apiService.requestDemand(id);
    }

    @Override
    public Call<MessageResponse> createDemand(String description, String title, String categoryId) {
        return apiService.createDemand(description, title, categoryId);
    }

    @Override
    public Call<MessageResponse> updateDemand(String id, String title, String description) {
        return apiService.updateDemand(id, title, description);
    }

    @Override
    public Call<ResponseBody> deleteDemand(String id) {
        return apiService.deleteDemand(id);
    }

    /* OFFER */

    @Override
    public Call<GetOffersResponse> getOffers(Integer size, String nextCursor, String prevCursor) {
        return apiService.getOffers(size, nextCursor, prevCursor);
    }

    @Override
    public Call<MessageResponse> requestOffer(String id) {
        return apiService.requestOffer(id);
    }

    @Override
    public Call<Void> createOffer(RequestBody description, RequestBody title, MultipartBody.Part image, RequestBody categoryId) {
        return apiService.createOffer(description, title, image, categoryId);
    }

    @Override
    public Call<MessageResponse> updateOffer(RequestBody id, RequestBody title, RequestBody desc,
                                             MultipartBody.Part image, RequestBody categoryId) {
        return apiService.updateOffer(id, title, desc, image, categoryId);
    }

    @Override
    public Call<ResponseBody> deleteOffer(String id) {
        return apiService.deleteOffer(id);
    }



    /* USER */

    @Override
    public Call<User> getUsers(String filter) {
        return apiService.getUsers(filter);
    }

    @Override
    public Call<MessageResponse> registerUser(Integer phone, Integer yearOfBirth, Boolean isElder) {
        return apiService.registerUser(phone, yearOfBirth, isElder);
    }

    @Override
    public Call<LoginResponse> loginUser(Integer phone, String hash) {
        return apiService.loginUser(phone, hash);
    }

    @Override
    public Call<User> getUserProfile() {
        return apiService.getUserProfile();
    }

    @Override
    public Call<List<OfferUser>> getUserOffers() {
        return apiService.getUserOffers();
    }

    @Override
    public Call<List<DemandUser>> getUserDemands() {
        return apiService.getUserDemands();
    }

    @Override
    public Call<MessageResponse> updateUser(RequestBody firstName, RequestBody lastName, RequestBody description, MultipartBody.Part image) {
        return apiService.updateUser(firstName, lastName, description, image);
    }

    @Override
    public Call<List<Category>> getCategories() {
        return apiService.getCategories();
    }


    @Override
    public Call<List<RequestResponse>> getChatRequests(){ return apiService.getChatRequests(); }

    @Override
    public Call<MessageResponse> acceptOffer(String requestId) {
        return apiService.acceptOffer(requestId);
    }

    @Override
    public Call<MessageResponse> acceptDemand(String requestId) {
        return apiService.acceptDemand(requestId);
    }

}
