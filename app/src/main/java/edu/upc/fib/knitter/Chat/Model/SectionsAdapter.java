package edu.upc.fib.knitter.Chat.Model;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import edu.upc.fib.knitter.Chat.ChatsFragment;
import edu.upc.fib.knitter.Chat.RequestsFragment;

public class SectionsAdapter extends FragmentPagerAdapter {

    public SectionsAdapter(FragmentManager fm){
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                RequestsFragment requestsFragment = new RequestsFragment();
                return requestsFragment;

            case 1:
                ChatsFragment chatsFragment = new ChatsFragment();
                return chatsFragment;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    public CharSequence getPageTitle(int position){
        switch(position){
            case 0:
                return "SOLICITUDES";

            case 1:
                return "CHATS";

            default:
                return null;
        }
    }
}
