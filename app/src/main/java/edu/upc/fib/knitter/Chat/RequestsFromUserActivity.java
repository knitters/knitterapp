package edu.upc.fib.knitter.Chat;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.Chat.Model.ChatUser;
import edu.upc.fib.knitter.Chat.Model.ChatUserAdapter;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.RequestResponse;
import edu.upc.fib.knitter.ui.view.BaseActivity;
import edu.upc.fib.knitter.usecase.AcceptDemand;
import edu.upc.fib.knitter.usecase.RequestChats;
import retrofit2.Call;
import retrofit2.Response;

public class RequestsFromUserActivity extends BaseActivity implements RequestsFromUserPresenter.View {

    private View mainView;
    private RecyclerView requestsList;
    private ChatUserAdapter chatUserAdapter;
    private List<RequestResponse> lRequests;
    private RecyclerView.LayoutManager layoutManager;
    private String ownId, otherId;


    private String currentUser;

    @Inject
    RequestsFromUserPresenter presenter;

    private void initializePresenter(){
        presenter.initialize();
        presenter.setView(this);
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();
        setContentView(R.layout.activity_requests_from_user);

        requestsList = findViewById(R.id.user_requests_recycler);
        requestsList.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(this);
        requestsList.setLayoutManager(layoutManager);

        ownId = getIntent().getStringExtra("own_phone");
        otherId = getIntent().getStringExtra("other_phone");

        //TODO adapter for the requests


    }


    @Override
    public void showOfferAccepted(){
        Toast.makeText(this, "Oferta Aceptada", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDemandAccepted(){
        Toast.makeText(this, "Demanda Aceptada", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sendRequestInfo(List<RequestResponse> list){
        this.lRequests = list;
        if(lRequests != null) {
            MyAdapter adapter = new MyAdapter(this, lRequests);
            requestsList.setAdapter(adapter);
        }
    }


    @Override
    public int getLayoutId() {
        return R.layout.activity_requests_from_user;
    }


    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

        private List<RequestResponse> list;
        private Context context;

        public MyAdapter(Context context, List<RequestResponse> list){
            this.list = list;
            this.context = context;
        }


        @NonNull
        @Override
        public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(context).inflate(R.layout.request_single, viewGroup, false);
            //ID PARA LLAMAR A LA API
            final String id = list.get(i).getId();
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    String[] options = {"Aceptar","Rechazar","Atras"};
                    builder.setItems(options, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch(which) {
                                case 0:
                                    //TODO call api and acept request
                                    //presenter.acceptDemand(id);
                                    //presenter.acceptOffer(id);
                                    Toast.makeText(context, "Aceptar", Toast.LENGTH_SHORT).show();
                                    break;
                                case 1:
                                    //TODO call api and delete request
                                    Toast.makeText(context, "Rechazar", Toast.LENGTH_SHORT).show();
                                    break;
                                case 2:
                                    break;
                            }
                        }
                    });
                    builder.show();

                }
            });
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
            RequestResponse request = list.get(i);

            if(request != null) {
                String author = request.getAuthor();
                String title = request.getOfferRequest().getTitle();

                myViewHolder.tvName.setText(title);

                if (author == ownId) myViewHolder.tvTipo.setText("Solicitud");
                else myViewHolder.tvTipo.setText("Peticion");
            }
        }

        @Override
        public int getItemCount() {
            return list.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            //data items

            public TextView tvTipo;
            public TextView tvName;


            public MyViewHolder(@NonNull View v){
                super(v);
                this.tvTipo = v.findViewById(R.id.tv_tipo_solicitud);
                this.tvName = v.findViewById(R.id.tv_nombre_solicitud);
            }
        }

    }



}
