package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class DeletePetition {

    private final ApiRepository repository;

    @Inject
    public DeletePetition(ApiRepository repository) {
        this.repository = repository;
    }

    public interface Callback {
        void onResponse(Call<ResponseBody> call, Response<ResponseBody> response);
        void onFailure(Call<ResponseBody> call, Throwable t);
    }

    public void doDelete(final String id, final DeletePetition.Callback callback){
        new Thread(new Runnable() {
            @Override public void run() {
                Call<ResponseBody> response = repository.deleteDemand(id);
                response.enqueue(new retrofit2.Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }
}
