package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.GetDemandsResponse;
import edu.upc.fib.knitter.apiclient.model.GetDemandsUserResponse;
import retrofit2.Call;
import retrofit2.Response;

public class GetDemands {

    private final ApiRepository repository;

    @Inject
    public GetDemands(ApiRepository repository) {
        this.repository = repository;
    }

    public void getDemands(final int numElements, final String prevCursor, final String nextCursor, final Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<GetDemandsResponse> response = repository.getDemands(numElements, prevCursor, nextCursor);
                response.enqueue(new retrofit2.Callback<GetDemandsResponse>() {
                    @Override
                    public void onResponse(Call<GetDemandsResponse> call, Response<GetDemandsResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<GetDemandsResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<GetDemandsResponse> call, Response<GetDemandsResponse> response);
        void onFailure(Call<GetDemandsResponse> call, Throwable t);
    }
}