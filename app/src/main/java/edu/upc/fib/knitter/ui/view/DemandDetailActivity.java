package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.ui.presenter.DemandDetailPresenter;

public class DemandDetailActivity extends BaseActivity implements DemandDetailPresenter.View {

    private static final String DEMAND_OBJECT_KEY = "demand_object_key";

    @Inject
    DemandDetailPresenter presenter;

    @BindView(R.id.tv_petition_author) TextView authorNameTextView;
    @BindView(R.id.tv_petition_description) TextView petitionDescriptionTextView;
    @BindView(R.id.bt_petition_request) Button requestButton;


    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestDemand();
            }
        });
    }

    @Override public int getLayoutId() {
        return R.layout.demand_detail;
    }

    @Override public void showDemand(Demand demand) {
        authorNameTextView.setText(demand.getAuthor().getFirstName());
        petitionDescriptionTextView.setText(demand.getDescription());
    }

    @Override
    public void onRequestSuccess() {
        Toast.makeText(this,"Requested Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestFailure() {
        Toast.makeText(this,"You already have requested this", Toast.LENGTH_SHORT).show();
    }

    public static void open(Context context, String demandJson) {
        Intent intent = new Intent(context, DemandDetailActivity.class);
        intent.putExtra(DEMAND_OBJECT_KEY, demandJson);
        context.startActivity(intent);
    }

    @Override protected void initializeToolbar() {
        super.initializeToolbar();
        setTitle(getDemand().getTitle());
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.setDemand(getDemand());
        presenter.initialize();
    }

    private Demand getDemand() {
        return new Gson().fromJson(getIntent().getExtras()
                .getString(DEMAND_OBJECT_KEY, ""), Demand.class);
    }
}
