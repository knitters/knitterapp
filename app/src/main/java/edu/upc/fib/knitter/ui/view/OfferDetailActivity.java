package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.ui.presenter.OfferDetailPresenter;

public class OfferDetailActivity extends BaseActivity implements OfferDetailPresenter.View {

    private static final String OFFER_OBJECT_KEY = "offer_object_key";

    @Inject OfferDetailPresenter presenter;

    @BindView(R.id.iv_offer_photo) ImageView offerPhotoImageView;
    @BindView(R.id.tv_offer_rating)  RatingBar ratingBar;
    @BindView(R.id.tv_offer_author) TextView offerAuthorTextView;
    @BindView(R.id.tv_offer_price) TextView offerPriceTextView;
    @BindView(R.id.tv_offer_description) TextView offerDescriptionTextView;
    @BindView(R.id.bt_offer_request) Button requestButton;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();

        requestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.requestOffer();
            }
        });
    }

    @Override public int getLayoutId() {
        return R.layout.offer_detail;
    }

    @Override public void showOffer(Offer offer) {
        if (!offer.getImagePath().isEmpty()) {
            Picasso.get().load(offer.getImagePath()).fit().centerCrop().into(offerPhotoImageView);
        }
        ratingBar.setRating((float) 3.5);
        offerAuthorTextView.setText(offer.getAuthor().getFirstName());
//        offerPriceTextView.setText(offer.getPrice());
        offerDescriptionTextView.setText(offer.getDescription());
    }

    @Override
    public void onRequestSuccess() {
        Toast.makeText(this,"Requested Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestFailure() {
        Toast.makeText(this,"You already have requested this", Toast.LENGTH_SHORT).show();
    }

    public static void open(Context context, String offerJson) {
        Intent intent = new Intent(context, OfferDetailActivity.class);
        intent.putExtra(OFFER_OBJECT_KEY, offerJson);
        context.startActivity(intent);
    }

    @Override protected void initializeToolbar() {
        super.initializeToolbar();
        Offer offer = getOffer();
        setTitle(offer.getTitle());
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.setOffer(getOffer());
        presenter.initialize();
    }

    private Offer getOffer() {
        return new Gson().fromJson(getIntent().getExtras()
                .getString(OFFER_OBJECT_KEY, ""), Offer.class);
    }
}
