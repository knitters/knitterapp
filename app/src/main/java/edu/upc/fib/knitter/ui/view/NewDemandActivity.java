package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Category;
import edu.upc.fib.knitter.ui.presenter.NewDemandPresenter;

public class NewDemandActivity extends BaseActivity implements NewDemandPresenter.View {

    @Inject
    NewDemandPresenter presenter;

    @BindView(R.id.et_demand_name) EditText demandName;
    @BindView(R.id.et_demand_description) EditText demandDescription;
    @BindView(R.id.s_category) Spinner sCategory;
    @BindView(R.id.bt_demand_add) Button demandAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();
        setTitle(this.getResources().getString(R.string.new_demand));

        demandAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = demandName.getText().toString();
                String description = demandDescription.getText().toString();
                if (name.equals("")) {
                    showMessageInt(R.string.empty_title);
                }
                else if(description.equals("")){
                    showMessageInt(R.string.empty_desc);
                }
                else {
                    Category category = (Category) sCategory.getSelectedItem();
                    presenter.postDemand(description, name, category.getId());
                }
            }
        });

        presenter.getCategories();
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, NewDemandActivity.class);
        context.startActivity(intent);
    }

    private void initializePresenter() {
        presenter.setView(this);
    }


    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    @Override
    public void setCategorySpinner(List<Category> categories) {
        ArrayAdapter categoryAdapter = new ArrayAdapter(this, R.layout.spinner, categories);
        sCategory.setAdapter(categoryAdapter);
        hideLoading();
    }

    @Override
    public int getLayoutId() {
        return R.layout.demand_new;
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void showMessageString(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessageInt(int message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToMain() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}