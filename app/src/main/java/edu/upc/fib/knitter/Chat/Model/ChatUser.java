package edu.upc.fib.knitter.Chat.Model;

public class ChatUser {


    public String name;
    public String image;
    public String id;
    public String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ChatUser(){

    }

    public ChatUser(String id, String phone, String name, String image) {
        this.id = id;
        this.phone = phone;
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }




}
