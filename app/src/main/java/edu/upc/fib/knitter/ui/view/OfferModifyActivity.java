package edu.upc.fib.knitter.ui.view;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Offer;
import edu.upc.fib.knitter.ui.presenter.OfferModifyPresenter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class OfferModifyActivity extends BaseActivity implements OfferModifyPresenter.View {
    
    @Inject
    OfferModifyPresenter presenter;

    @BindView(R.id.iv_offer_photo) ImageView offerPhotoImageView;
    @BindView(R.id.tv_offer_rating)  RatingBar ratingBar;
    @BindView(R.id.et_offer_title) EditText offerTitleEditText;
    @BindView(R.id.et_offer_description) EditText offerDescriptionEditText;
    @BindView(R.id.offerImage) ImageView imageV;
    @BindView(R.id.ImgBtn) Button image;
    @BindView(R.id.Save) Button save;
    @BindView(R.id.deleteButton) ImageButton delete;

    private static final String OFFER_OBJECT_KEY = "offer_object_key";

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    String mCurrentPhotoPath;
    String mCurrentPhotoName;
    String scalledImageFileName;
    File scalledImageFile;
    RequestBody requestFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImage();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tit = offerTitleEditText.getText().toString();
                String det = offerDescriptionEditText.getText().toString();

                if (!showEmptyErrors(tit, det)) {
                    //                requestFile = RequestBody.create(MediaType.parse("image"), new File(mCurrentPhotoPath));
                    requestFile = RequestBody.create(MediaType.parse("image/*"), scalledImageFile);
                    MultipartBody.Part image = MultipartBody.Part.createFormData("image", scalledImageFileName, requestFile);

                    presenter.doSave(tit, det, image);
                }
                //presenter.doSave(offerDescriptionEditText.getText().toString(), );
            }
        });

        //offerPhotoImageView.setOnClickListener(new View.OnClickListener(){
        //    @Override
        //    public void onClick(View view) {
        //        openPicture();
        //    }
        //});

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.doDelete();
            }
        });
    }

    private boolean showEmptyErrors(String tit, String det) {
        boolean empty = false;
        if (tit.isEmpty()) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_title), Toast.LENGTH_SHORT).show();
        } else if (det.isEmpty()) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_desc), Toast.LENGTH_SHORT).show();
        } else if (scalledImageFile == null) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_image), Toast.LENGTH_SHORT).show();
        }
        return empty;
    }

    @Override public int getLayoutId() {
        return R.layout.offer_modify;
    }

    private void getImage() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialog);
        builder.setTitle("Get an Image");
        builder.setMessage("Choose");
        builder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                getPhotoFromCamera();
            }
        });
        builder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                try {
                    getPhotoFromStorage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.show();
    }

    private void getPhotoFromCamera () {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                scalledImageFile = createImageFile();
                scalledImageFileName = scalledImageFile.getName();

            } catch (IOException ex) {
                // Error occurred while creating the File

            }

            // Continue only if the File was successfully created
            if (scalledImageFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "edu.upc.fib.knitter",
                        scalledImageFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    // Make the system add the image to the gallery
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    // Scale down the image
    private void setPic() throws FileNotFoundException {
        // Get the dimensions of the View
        int targetW = imageV.getWidth();
        int targetH = imageV.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        FileOutputStream fos = new FileOutputStream(scalledImageFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);

        imageV.setImageBitmap(bitmap);
    }
    private void setPicFromGallery(Uri uri) throws FileNotFoundException {
        // Get the dimensions of the View
        int targetW = imageV.getWidth();
        int targetH = imageV.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        //Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        InputStream is = getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(is, null, bmOptions);

        FileOutputStream fos = new FileOutputStream(scalledImageFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);

        imageV.setImageBitmap(bitmap);
    }

    private void getPhotoFromStorage() throws IOException {
        scalledImageFile = createImageFile();
        scalledImageFileName = scalledImageFile.getName();

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_IMAGE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            galleryAddPic();
            try {
                setPic();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            try {
                setPicFromGallery(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        mCurrentPhotoName = imageFileName;
        scalledImageFile = File.createTempFile(
                imageFileName + "-small",  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    @Override public void showOffer(Offer offer) {
        if (!offer.getImagePath().isEmpty()) {
            Picasso.get().load(offer.getImagePath()).fit().centerCrop().into(offerPhotoImageView);
        }
        ratingBar.setRating((float) 3.5);
        //offerPriceEditText.setText(offer.getPrice());
        offerTitleEditText.setText(offer.getTitle());
        offerDescriptionEditText.setText(offer.getDescription());
    }

    public static void open(Context context, String offerJson) {
        Intent intent = new Intent(context, OfferModifyActivity.class);
        intent.putExtra(OFFER_OBJECT_KEY, offerJson);
        context.startActivity(intent);
    }

    @Override protected void initializeToolbar() {
        super.initializeToolbar();
        setTitle(getString(R.string.edit_offer_title));
    }

    @Override public void onSaveSuccess() {
        Toast.makeText(this, "Success on Save",Toast.LENGTH_LONG).show();
    }
    @Override public void onSaveFailure() {
        Toast.makeText(this, "Failed on Save",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteFailure() {
        Toast.makeText(this, "Failed on Delete",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteSuccess() {
        UserOffersActivity.open(this);
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.setOffer(getOffer());
        presenter.initialize();
    }

    private Offer getOffer() {
        return new Gson().fromJson(getIntent().getExtras()
                .getString(OFFER_OBJECT_KEY, ""), Offer.class);
    }
}
