package edu.upc.fib.knitter.ui.view;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Category;
import edu.upc.fib.knitter.ui.presenter.NewOfferPresenter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class NewOfferActivity extends BaseActivity implements NewOfferPresenter.View {

    @Inject
    NewOfferPresenter presenter;

    @BindView(R.id.categorySpinner) Spinner category;
    @BindView(R.id.titleEditText) EditText title;
    @BindView(R.id.detailsEditText) EditText details;
    @BindView(R.id.offerImage) ImageView imageV;
    @BindView(R.id.ImgBtn) Button image;
    @BindView(R.id.benefitEditText) EditText benefit;
    @BindView(R.id.publishButton) Button publish;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    String mCurrentPhotoPath;
    String mCurrentPhotoName;
    String scalledImageFileName;
    File scalledImageFile;
    RequestBody requestFile;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();
        setTitle(this.getResources().getString(R.string.new_offer));

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getImage();
            }
        });

        publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Category cat = null;
                cat = (Category) category.getSelectedItem();
                String tit = title.getText().toString();
                String det = details.getText().toString();

                if (!showEmptyErrors(cat, tit, det)) {
                    //                requestFile = RequestBody.create(MediaType.parse("image"), new File(mCurrentPhotoPath));
                    requestFile = RequestBody.create(MediaType.parse("image/*"), scalledImageFile);
                    MultipartBody.Part image = MultipartBody.Part.createFormData("image", scalledImageFileName, requestFile);

                    presenter.postOffer(det, tit, image, cat.getId());
                }
            }
        });
        presenter.getCategories();
        hideLoading();
    }

    @Override public int getLayoutId() {
        return R.layout.offer_new;
    }

    private void getImage () {
        AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AlertDialog);
        builder.setTitle("Get an Image");
        builder.setMessage("Choose");
        builder.setPositiveButton("Camera", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                getPhotoFromCamera();
            }
        });
        builder.setNegativeButton("Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                try {
                    getPhotoFromStorage();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        builder.show();
    }

    private void getPhotoFromCamera () {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            try {
                scalledImageFile = createImageFile();
                scalledImageFileName = scalledImageFile.getName();

            } catch (IOException ex) {
                // Error occurred while creating the File

            }

            // Continue only if the File was successfully created
            if (scalledImageFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "edu.upc.fib.knitter",
                        scalledImageFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    // Make the system add the image to the gallery
    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        this.sendBroadcast(mediaScanIntent);
    }

    // Scale down the image
    private void setPic() throws FileNotFoundException {
        // Get the dimensions of the View
        int targetW = imageV.getWidth();
        int targetH = imageV.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);

        FileOutputStream fos = new FileOutputStream(scalledImageFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);

        imageV.setImageBitmap(bitmap);
    }
    private void setPicFromGallery(Uri uri) throws FileNotFoundException {
        // Get the dimensions of the View
        int targetW = imageV.getWidth();
        int targetH = imageV.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        //Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        InputStream is = getContentResolver().openInputStream(uri);
        Bitmap bitmap = BitmapFactory.decodeStream(is, null, bmOptions);

        FileOutputStream fos = new FileOutputStream(scalledImageFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);

        imageV.setImageBitmap(bitmap);
    }

    private void getPhotoFromStorage() throws IOException {
        scalledImageFile = createImageFile();
        scalledImageFileName = scalledImageFile.getName();

        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(pickPhoto, REQUEST_IMAGE_GALLERY);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            galleryAddPic();
            try {
                setPic();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            Uri selectedImage = data.getData();
            try {
                setPicFromGallery(selectedImage);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        mCurrentPhotoName = imageFileName;
        scalledImageFile = File.createTempFile(
                imageFileName + "-small",  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        return image;
    }

    @Override public void goToMenu() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, NewOfferActivity.class);
        context.startActivity(intent);
    }

    @Override public void showMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setCategorySpinner(List<Category> categories) {
        ArrayAdapter categoryAdapter = new ArrayAdapter(this, R.layout.spinner, categories);
        category.setAdapter(categoryAdapter);
        hideLoading();
    }

    private boolean showEmptyErrors(Category cat, String tit, String det) {
        boolean empty = false;
        if (tit.isEmpty()) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_title), Toast.LENGTH_SHORT).show();
        } else if (det.isEmpty()) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_desc), Toast.LENGTH_SHORT).show();
        } else if (cat==null) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_cat), Toast.LENGTH_SHORT).show();
        } else if (scalledImageFile == null) {
            empty = true;
            Toast.makeText(this, getString(R.string.empty_image), Toast.LENGTH_SHORT).show();
        }
        return empty;
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.initialize();
    }
}