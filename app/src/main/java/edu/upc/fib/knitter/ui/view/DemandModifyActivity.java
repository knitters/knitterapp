package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.ui.presenter.DemandModifyPresenter;

public class DemandModifyActivity extends BaseActivity implements DemandModifyPresenter.View {

    private static final String DEMAND_OBJECT_KEY = "demand_object_key";

    @Inject
    DemandModifyPresenter presenter;

    @BindView(R.id.et_demand_description) EditText demandDescriptionEditText;
    @BindView(R.id.Save) Button save;
    @BindView(R.id.deleteButton) ImageButton delete;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.doSave(demandDescriptionEditText.getText().toString());
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.doDelete();
            }
        });

    }

    @Override public int getLayoutId() {
        return R.layout.demand_modify;
    }

    @Override public void showDemand(DemandUser demand) {
        demandDescriptionEditText.setText(demand.getDescription());
    }

    public static void open(Context context, String demandJson) {
        Intent intent = new Intent(context, DemandModifyActivity.class);
        intent.putExtra(DEMAND_OBJECT_KEY, demandJson);
        context.startActivity(intent);
    }

    @Override protected void initializeToolbar() {
        super.initializeToolbar();
        setTitle(getDemand().getTitle());
    }

    @Override
    public void onSaveFailure() {
        Toast.makeText(this, "Failed on Save",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSaveSuccess() {
        Toast.makeText(this, "Success on Save",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteFailure() {
        Toast.makeText(this, "Failed on Delete",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDeleteSuccess() {
        UserDemandsActivity.open(this, true);
        finish();
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        presenter.setDemand(getDemand());
        presenter.initialize();
    }

    private DemandUser getDemand() {
        return new Gson().fromJson(getIntent().getExtras()
                .getString(DEMAND_OBJECT_KEY, ""), DemandUser.class);
    }
}
