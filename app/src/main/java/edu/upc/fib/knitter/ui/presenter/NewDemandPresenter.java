package edu.upc.fib.knitter.ui.presenter;

import android.content.Context;
import android.util.Log;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.Category;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.usecase.GetCategories;
import edu.upc.fib.knitter.usecase.NewDemand;
import retrofit2.Call;
import retrofit2.Response;

public class NewDemandPresenter extends Presenter<NewDemandPresenter.View> {

    private final NewDemand newDemand;
    private final GetCategories getCategories;

    @Inject
    public NewDemandPresenter(NewDemand newDemand, GetCategories getCategories) {
        this.newDemand = newDemand;
        this.getCategories = getCategories;
    }

    public void postDemand(String description, String title, String categoryId) {
        newDemand.postDemand(description, title, categoryId, new NewDemand.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                if (response.isSuccessful()) {
                    getView().showMessageInt(R.string.Demand_OK);
                    getView().navigateToMain();
                } else {
                    // error response, no access to resource?
                    getView().showMessageInt(R.string.Demand_KO);
                }
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessageString(t.getMessage());
            }
        });
    }

    public void getCategories() {
        getCategories.getCategories(new GetCategories.Callback() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                if (response.isSuccessful()) {
                    getView().setCategorySpinner(response.body());
                } else {
                    // error response, no access to resource?
                    getView().navigateToMain();
                    getView().showMessageInt(R.string.Categories_KO);
                }
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                // something went completely south (like no internet connection)
                Log.d("Error", t.getMessage());
                getView().showMessageString(t.getMessage());
            }
        });
    }

    public interface View extends Presenter.View {
        Context getContext();
        void showMessageString(String message);
        void showMessageInt(int message);
        void navigateToMain();
        void setCategorySpinner(List<Category> categories);
    }
}