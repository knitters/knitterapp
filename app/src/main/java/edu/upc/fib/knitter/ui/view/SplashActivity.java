package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.OnClick;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.ui.presenter.SplashPresenter;

public class SplashActivity extends BaseActivity implements SplashPresenter.View {

    private SharedPreferences mPrefs;

    @BindView(R.id.status_message) TextView tvStatusMessage;
    @BindView(R.id.b_retry) Button bRetry;

    @Inject SplashPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        initializePresenter();
        presenter.initialize();

        mPrefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);
    }

    @Override public int getLayoutId(){
        return R.layout.activity_splash;
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
    }

    public void navigateToMainMenu() {
        Intent intent = new Intent(this, MainMenuActivity.class);
        startActivity(intent);
    }

    public void navigateToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void serverOk() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!mPrefs.getString("token", "").isEmpty()) {
                    navigateToMainMenu();
                }
                else {
                    navigateToLogin();
                }
            }
        }, 1000);
    }

    @Override
    public void serverKo() {
        tvStatusMessage.setText(getString(R.string.server_generic_error));
        bRetry.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.b_retry)
    void retry() {
        tvStatusMessage.setText(getString(R.string.loading));
        bRetry.setVisibility(View.GONE);
        presenter.checkServerStatus();
    }
}
