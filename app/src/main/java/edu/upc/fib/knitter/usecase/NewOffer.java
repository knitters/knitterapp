package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class NewOffer {

    private final ApiRepository repository;

    @Inject
    public NewOffer(ApiRepository repository) {
        this.repository = repository;
    }

    public void postOffer(final RequestBody description, final RequestBody title,
                          final MultipartBody.Part photo, final RequestBody categoryId,
                          final Callback callback) {

        new Thread(new Runnable() {
            @Override public void run() {

                Call<Void> response = repository.createOffer(description, title, photo, categoryId);
                response.enqueue(new retrofit2.Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<Void> call, Response<Void> response);

        void onFailure(Call<Void> call, Throwable t);
    }
}