package edu.upc.fib.knitter.usecase;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.OfferUser;
import retrofit2.Call;
import retrofit2.Response;

public class GetUserOffers {

    private final ApiRepository repository;

    @Inject
    public GetUserOffers(ApiRepository repository) {
        this.repository = repository;
    }

    public void getUserOffers(final GetUserOffers.Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<List<OfferUser>> response = repository.getUserOffers();
                response.enqueue(new retrofit2.Callback<List<OfferUser>>() {
                    @Override
                    public void onResponse(Call<List<OfferUser>> call, Response<List<OfferUser>> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<List<OfferUser>> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<List<OfferUser>> call, Response<List<OfferUser>> response);
        void onFailure(Call<List<OfferUser>> call, Throwable t);
    }

}
