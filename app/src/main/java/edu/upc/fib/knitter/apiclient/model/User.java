package edu.upc.fib.knitter.apiclient.model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id") String id;
    @SerializedName("phoneNumber") Integer phoneNumber;
    @SerializedName("firstName") String firstName;
    @SerializedName("lastName") String lastName;
    @SerializedName("imagePath") String imagePath;
    @SerializedName("description") String description;
    @SerializedName("isElder") Boolean isElder;

    public User(String id, Integer phoneNumber, String firstName, String lastName, String imagePath, String description, Boolean isElder) {
        this.id = id;
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.imagePath = imagePath;
        this.description = description;
        this.isElder = isElder;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(Integer phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getElder() {
        return isElder;
    }

    public void setElder(Boolean elder) {
        isElder = elder;
    }

}
