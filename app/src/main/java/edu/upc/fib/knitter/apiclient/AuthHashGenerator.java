package edu.upc.fib.knitter.apiclient;

import java.security.MessageDigest;

public class AuthHashGenerator {
//    String generateHashMD5(String timestamp, String publicKey, String privateKey)
//            throws KnitterApiException {
//        try {
//            String value = timestamp + privateKey + publicKey;
//            MessageDigest md5Encoder = MessageDigest.getInstance("MD5");
//            byte[] md5Bytes = md5Encoder.digest(value.getBytes());
//
//            StringBuilder md5 = new StringBuilder();
//            for (int i = 0; i < md5Bytes.length; ++i) {
//                md5.append(Integer.toHexString((md5Bytes[i] & 0xFF) | 0x100).substring(1, 3));
//            }
//            return md5.toString();
//        } catch (NoSuchAlgorithmException e) {
//            throw new KnitterApiException("cannot generate the api key", e);
//        }
//    }

    public static String generateHashSHA256(String input) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes("UTF-8"));
            StringBuffer hexString = new StringBuffer();

            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch(Exception ex) {
            throw new RuntimeException(ex);
        }
    }
}
