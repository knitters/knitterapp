package edu.upc.fib.knitter.usecase;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.RequestResponse;
import retrofit2.Call;
import retrofit2.Response;

public class RequestChats {

    private final ApiRepository repository;

    @Inject
    public RequestChats(ApiRepository repository){
        this.repository = repository;
    }

    public void getChats(final RequestChats.Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<List<RequestResponse>> response = repository.getChatRequests();
                response.enqueue(new retrofit2.Callback<List<RequestResponse>>() {
                    @Override
                    public void onResponse(Call<List<RequestResponse>> call, Response<List<RequestResponse>> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<List<RequestResponse>> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<List<RequestResponse>> call, Response<List<RequestResponse>> response);
        void onFailure(Call<List<RequestResponse>> call, Throwable t);
    }
}
