package edu.upc.fib.knitter.Chat;

import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.apiclient.model.RequestResponse;
import edu.upc.fib.knitter.ui.presenter.Presenter;
import edu.upc.fib.knitter.usecase.AcceptDemand;
import edu.upc.fib.knitter.usecase.AcceptOffer;
import edu.upc.fib.knitter.usecase.RequestChats;
import retrofit2.Call;
import retrofit2.Response;

public class RequestsFromUserPresenter extends Presenter<RequestsFromUserPresenter.View> {

    private final RequestChats requestChats;
    private final AcceptDemand acceptDemand;
    private final AcceptOffer acceptOffer;

    @Inject
    public RequestsFromUserPresenter(RequestChats requestChats, AcceptDemand acceptDemand, AcceptOffer acceptOffer){
        this.requestChats = requestChats;
        this.acceptDemand = acceptDemand;
        this.acceptOffer = acceptOffer;
    }

    @Override
    public void initialize(){
        super.initialize();
        getRequests();
    }

    public void acceptOffer(String id){
        acceptOffer.doAcceptOffer(id, new AcceptOffer.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {

            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {

            }
        });
    }

    public void acceptDemand(String id){
        acceptDemand.doAcceptDemand(id, new AcceptDemand.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {

            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {

            }
        });
    }

    private void getRequests(){
        requestChats.getChats(new RequestChats.Callback() {
            @Override
            public void onResponse(Call<List<RequestResponse>> call, Response<List<RequestResponse>> response) {
                List<RequestResponse> r = response.body();
                getView().sendRequestInfo(r);
            }

            @Override
            public void onFailure(Call<List<RequestResponse>> call, Throwable t) {

            }
        });

    }

    public interface View extends Presenter.View{

        void sendRequestInfo(List<RequestResponse> list);

        void showOfferAccepted();

        void showDemandAccepted();
    }
}
