package edu.upc.fib.knitter.di;

import javax.inject.Singleton;

import dagger.Component;
import edu.upc.fib.knitter.Chat.RequestsFromUserActivity;
import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.ui.view.DemandDetailActivity;
import edu.upc.fib.knitter.ui.view.DemandModifyActivity;
import edu.upc.fib.knitter.ui.view.DemandsFeedActivity;
import edu.upc.fib.knitter.ui.view.LoginActivity;
import edu.upc.fib.knitter.ui.view.MainMenuActivity;
import edu.upc.fib.knitter.ui.view.MyProfileViewActivity;
import edu.upc.fib.knitter.ui.view.NewDemandActivity;
import edu.upc.fib.knitter.ui.view.NewOfferActivity;
import edu.upc.fib.knitter.ui.view.OfferDetailActivity;
import edu.upc.fib.knitter.ui.view.OfferModifyActivity;
import edu.upc.fib.knitter.ui.view.OffersFeedActivity;
import edu.upc.fib.knitter.ui.view.PendingDemandListActivity;
import edu.upc.fib.knitter.ui.view.ProfileModifyActivity;
import edu.upc.fib.knitter.ui.view.ProfileViewActivity;
import edu.upc.fib.knitter.ui.view.RegisterActivity;
import edu.upc.fib.knitter.ui.view.SplashActivity;
import edu.upc.fib.knitter.ui.view.UserDemandsActivity;
import edu.upc.fib.knitter.ui.view.UserOffersActivity;

@Singleton @Component(modules = {MainModule.class, NetworkModule.class})
public interface MainComponent {

    void inject(MainMenuActivity activity);
    void inject(OffersFeedActivity activity);
    void inject(UserOffersActivity activity);
    void inject(OfferDetailActivity activity);
    void inject(OfferModifyActivity activity);
    void inject(DemandDetailActivity activity);
    void inject(DemandsFeedActivity demandsFeedActivity);
    void inject(UserDemandsActivity activity);
    void inject(DemandModifyActivity modifyDemandActivity);
    void inject(NewDemandActivity activity);
    void inject(MyProfileViewActivity myProfileViewActivity);
    void inject(ProfileViewActivity activity);
    void inject(NewOfferActivity activity);
    void inject(ProfileModifyActivity profileModifyActivity);
    void inject(RegisterActivity activity);
    void inject(SplashActivity splashActivity);
    void inject(LoginActivity loginActivity);
    void inject(PendingDemandListActivity pendingDemandListActivity);
    void inject(ApiRepository repository);
    void inject(RequestsFromUserActivity requestsFromUserActivity);
}
