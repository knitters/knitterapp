package edu.upc.fib.knitter.ui.view;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.upc.fib.knitter.Chat.RequestsFromUserActivity;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.Chat.Model.ChatMessage;
import edu.upc.fib.knitter.Chat.Model.ChatMessageAdapter;

public class ChatActivity extends BaseActivity {

    private String userid;
    private DatabaseReference rootReference;

    ImageButton btn_send;
    EditText text_send;

    ChatMessageAdapter messageAdapter;
    List<ChatMessage> lChat;

    RecyclerView recyclerView;

    //TODO en onstart de cada activity, si el user es nulo, redirigirlo al login 'sendtostart'

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        Toolbar toolbar = findViewById(R.id.chat_main_toolbar);
        //TODO custom toolbar

        final FirebaseUser fuser = FirebaseAuth.getInstance().getCurrentUser();
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        recyclerView = findViewById(R.id.recycler_chat);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        userid = getIntent().getStringExtra("chat_user_id");

        btn_send = findViewById(R.id.btn_send);
        text_send = findViewById(R.id.text_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = text_send.getText().toString();
                if(!message.equals("")){
                    assert userid != null;
                    sendMessage(fuser.getUid(), userid, message);
                    text_send.setText("");
                }
                else{
                    Toast.makeText(ChatActivity.this, "You can't send an empty message", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rootReference = FirebaseDatabase.getInstance().getReference();
        final DatabaseReference userReference = rootReference.child("Users").child(userid);

        userReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String username = dataSnapshot.child("name").getValue(String.class);
                getSupportActionBar().setTitle(username);
                //TODO set user image
                readMessages(fuser.getUid(), userid, "default");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.chat_main_menu, menu);

        MenuItem item = menu.getItem(0);
        SpannableString s = new SpannableString("Ver solicitudes");
        s.setSpan(new ForegroundColorSpan(Color.BLACK), 0, s.length(), 0);
        item.setTitle(s);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.chat) {
            Intent intent = new Intent(this, RequestsFromUserActivity.class);
            //intent.putExtra("")
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendMessage(String sender, String receiver, String message ){

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("sender",sender);
        hashMap.put("receiver",receiver);
        hashMap.put("message",message);
        reference.child("Chats").push().setValue(hashMap);

    }


    private void readMessages(final String myid, final String userid, String imageUrl){
        lChat = new ArrayList<>();

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chats");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                lChat.clear();
                for(DataSnapshot snapshot : dataSnapshot.getChildren()){
                    //myid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    ChatMessage message = new ChatMessage();
                    message.setMessage(snapshot.child("message").getValue(String.class));
                    message.setSender(snapshot.child("sender").getValue(String.class));
                    message.setReceiver(snapshot.child("receiver").getValue(String.class));

                    if(message.getReceiver().equals(myid) && message.getSender().equals(userid)
                            || message.getReceiver().equals(userid) && message.getSender().equals(myid)){
                        lChat.add(message);
                    }
                    //TODO set users image
                    messageAdapter = new ChatMessageAdapter(ChatActivity.this, lChat, "default");
                    recyclerView.setAdapter(messageAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_chat;
    }
}
