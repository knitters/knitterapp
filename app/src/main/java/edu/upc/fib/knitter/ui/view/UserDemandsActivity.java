package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageButton;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import edu.upc.fib.knitter.apiclient.model.PaginationMetaData;
import edu.upc.fib.knitter.ui.presenter.UserDemandsPresenter;

public class UserDemandsActivity extends BaseActivity implements UserDemandsPresenter.View {

    @Inject
    UserDemandsPresenter presenter;

    private UserDemandsAdapter adapter;

    @BindView(R.id.tv_empty_case) View emptyCaseView;
    @BindView(R.id.recycler_view) RecyclerView recyclerView;

    @BindView(R.id.Prev) ImageButton prev;
    @BindView(R.id.Next) ImageButton next;

    SharedPreferences mPrefs;
    SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPrefs = getSharedPreferences("Prefs", Context.MODE_PRIVATE);

        initializeDagger();
        initializePresenter();
        initializeAdapter();
        initializeRecyclerView();
        presenter.initialize();

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.prevPage();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.nextPage();
            }
        });
    }

    public static void open(Context context, boolean clear) {
        Intent intent = new Intent(context, UserDemandsActivity.class);
        //if (clear) intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    public int getLayoutId() {
        return R.layout.demands_feed;
    }

    @Override
    public void showDemands(List<DemandUser> demands, int nElem) {
        adapter.addAll(demands, nElem);
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getPageSize(){
        int pxToDp = this.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT;
        return this.getResources().getDisplayMetrics().heightPixels / pxToDp;
    }

    @Override
    public void showPrev(){
        prev.setVisibility(View.VISIBLE);
    }

    @Override
    public void hidePrev(){
        prev.setVisibility(View.GONE);
    }

    @Override
    public void showNext(){
        next.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNext(){
        next.setVisibility(View.GONE);
    }

    @Override
    public void openDemandScreen(DemandUser demand) {
        DemandModifyActivity.open(this, new Gson().toJson(demand));
    }

    @Override
    public void showEmptyCase() {
        emptyCaseView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyCase() {
        emptyCaseView.setVisibility(View.GONE);
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
    }

    private void initializeAdapter() {
        String userName = mPrefs.getString("firstName", getString(R.string.no_name));
        adapter = new UserDemandsAdapter(presenter, userName);
    }

    private void initializeRecyclerView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }
}