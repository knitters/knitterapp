package edu.upc.fib.knitter.ui.presenter;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.GetDemandsResponse;
import edu.upc.fib.knitter.apiclient.model.PaginationMetaData;
import edu.upc.fib.knitter.usecase.GetDemands;
import retrofit2.Call;
import retrofit2.Response;

public class DemandsPresenter extends Presenter<DemandsPresenter.View> {

    private final GetDemands getDemands;

    @Inject
    public DemandsPresenter(GetDemands getDemands) {
        this.getDemands = getDemands;
    }

    @Override public void initialize() {
        setPage(null, null);
    }

    public void setPage(String prevCursor, String nextCursor){
        super.initialize();
        final View view = getView();
        view.showLoading();

        int pageSize = getView().getPageSize();

        int numElements = pageSize/100;

        getDemands.getDemands(numElements, prevCursor, nextCursor, new GetDemands.Callback() {

            @Override
            public void onResponse(Call<GetDemandsResponse> call, Response<GetDemandsResponse> response) {
                view.hideLoading();
                if (response.isSuccessful() && response.body().getMetaData().getSize() != 0) {
                    if (response.body().getMetaData().isFirst()) {
                        view.hidePrev();
                    } else {
                        view.showPrev();
                    }
                    if (response.body().getMetaData().isLast()) {
                        view.hideNext();
                    } else {
                        view.showNext();
                    }
                    view.savePaginationMetaData(response.body().getMetaData());
                    view.showDemands(response.body().getDemands(), response.body().getMetaData().getSize());
                } else {
                    view.showEmptyCase();
                    view.hidePrev();
                    view.hideNext();
                }
            }

            @Override
            public void onFailure(Call<GetDemandsResponse> call, Throwable t) {
                view.hideLoading();
                view.hidePrev();
                view.hideNext();
                view.showEmptyCase();
            }
        });
    }

    public void onDemandClicked(Demand demand) {
        getView().openDemandScreen(demand);
    }

    public interface View extends Presenter.View {

        void showEmptyCase();

        void hideEmptyCase();

        void showPrev();

        void hidePrev();

        void showNext();

        void hideNext();

        int getPageSize();

        void showDemands(List<Demand> demands, int nElem);

        void openDemandScreen(Demand demand);

        void savePaginationMetaData(PaginationMetaData paginationMetaData);
    }
}