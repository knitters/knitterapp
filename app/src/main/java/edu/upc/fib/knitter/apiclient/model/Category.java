package edu.upc.fib.knitter.apiclient.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

public class Category implements Comparable<Category> {
    @SerializedName("id") private String id;
    @SerializedName("nom") private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int compareTo(@NonNull Category category) {
        return this.name.compareTo(category.getName());
    }

    @Override
    public String toString() {
        return this.name;
    }
}
