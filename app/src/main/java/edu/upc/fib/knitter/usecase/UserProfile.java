package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.User;
import retrofit2.Call;
import retrofit2.Response;

public class UserProfile {

    private final ApiRepository repository;

    @Inject
    public UserProfile(ApiRepository repository){
        this.repository = repository;
    }

    public void getProfile(final UserProfile.Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<User> response = repository.getUserProfile();
                response.enqueue(new retrofit2.Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<User> call, Response<User> response);
        void onFailure(Call<User> call, Throwable t);
    }
}