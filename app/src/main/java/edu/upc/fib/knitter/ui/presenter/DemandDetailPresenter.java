package edu.upc.fib.knitter.ui.presenter;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.model.Demand;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import edu.upc.fib.knitter.usecase.ModifyDemand;
import edu.upc.fib.knitter.usecase.RequestDemand;
import retrofit2.Call;
import retrofit2.Response;

public class DemandDetailPresenter extends Presenter<DemandDetailPresenter.View> {

    private Demand demand;
    private RequestDemand requestDemand;

    @Inject
    public DemandDetailPresenter(RequestDemand requestDemand) {
        this.requestDemand = requestDemand;
    }

    public void setDemand(Demand demand) {
        this.demand = demand;
    }

    public void requestDemand(){
        final DemandDetailPresenter.View view = getView();
        view.showLoading();
        requestDemand.requestDemand(this.demand.getId(), new RequestDemand.Callback() {
            @Override
            public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                view.hideLoading();
                view.onRequestSuccess();
            }

            @Override
            public void onFailure(Call<MessageResponse> call, Throwable t) {
                view.hideLoading();
                view.onRequestFailure();
            }
        });
    }

    @Override public void initialize() {
        super.initialize();
        View view = getView();
        view.hideLoading();
        view.showDemand(demand);
    }

    public interface View extends Presenter.View {
        void showDemand(Demand demand);
        void onRequestSuccess();
        void onRequestFailure();
    }
}
