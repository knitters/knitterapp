package edu.upc.fib.knitter.usecase;

import java.util.List;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.DemandUser;
import retrofit2.Call;
import retrofit2.Response;

public class GetUserDemands {

    private final ApiRepository repository;

    @Inject
    public GetUserDemands(ApiRepository repository) {
        this.repository = repository;
    }

    public void getUserDemands(final GetUserDemands.Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {
                Call<List<DemandUser>> response = repository.getUserDemands();
                response.enqueue(new retrofit2.Callback<List<DemandUser>>() {
                    @Override
                    public void onResponse(Call<List<DemandUser>> call, Response<List<DemandUser>> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<List<DemandUser>> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<List<DemandUser>> call, Response<List<DemandUser>> response);
        void onFailure(Call<List<DemandUser>> call, Throwable t);
    }

}
