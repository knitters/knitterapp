package edu.upc.fib.knitter.usecase;
import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import retrofit2.Call;
import retrofit2.Response;

public class Register {

    private final ApiRepository repository;

    @Inject
    public Register(ApiRepository repository){
        this.repository = repository;
    }

    public void registerUser(final int phone, final int yearOfBirth, final boolean isElder, final Callback callback) {
        new Thread(new Runnable() {
            @Override public void run() {

                Call<MessageResponse> response = repository.registerUser(phone, yearOfBirth, isElder);
                response.enqueue(new retrofit2.Callback<MessageResponse>() {
                    @Override
                    public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<MessageResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<MessageResponse> call, Response<MessageResponse> response);
        void onFailure(Call<MessageResponse> call, Throwable t);
    }
}
