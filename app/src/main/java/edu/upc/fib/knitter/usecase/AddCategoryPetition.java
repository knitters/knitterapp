package edu.upc.fib.knitter.usecase;

import edu.upc.fib.knitter.apiclient.model.Demand;

public class AddCategoryPetition {

    private Demand demand;
    private String categoria;

    AddCategoryPetition(Demand demand, String categoria){
        this.demand = demand;
        this.categoria = categoria;
    }

    public void doChange(){
        //TODO Call API and link categoria to petition
    }

}
