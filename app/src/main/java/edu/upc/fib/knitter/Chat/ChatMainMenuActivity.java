package edu.upc.fib.knitter.Chat;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.ui.view.BaseActivity;
import edu.upc.fib.knitter.ui.view.MainMenuActivity;
import edu.upc.fib.knitter.Chat.Model.SectionsAdapter;

public class ChatMainMenuActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    private ViewPager viewPager;
    private SectionsAdapter sectionsAdapter;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_main_menu);

        Toolbar toolbar = findViewById(R.id.chat_main_toolbar);
        //TODO custom toolbar
        setSupportActionBar(toolbar);
        //getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChatMainMenuActivity.this, MainMenuActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
        mAuth = FirebaseAuth.getInstance();

        //Tabs
        viewPager = findViewById(R.id.chat_main_view_pager);
        sectionsAdapter = new SectionsAdapter(getSupportFragmentManager());
        viewPager.setAdapter(sectionsAdapter);

        tabLayout = findViewById(R.id.main_menu_tab);
        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            Intent intent = new Intent(ChatMainMenuActivity.this, ChatLoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, ChatMainMenuActivity.class);
        context.startActivity(intent);
    }




    @Override
    public int getLayoutId() {
        return R.layout.activity_chat_main_menu;
    }

}
