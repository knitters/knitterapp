package edu.upc.fib.knitter.ui.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import edu.upc.fib.knitter.KnitterApplication;
import edu.upc.fib.knitter.R;
import edu.upc.fib.knitter.apiclient.model.User;
import edu.upc.fib.knitter.ui.presenter.MyProfileViewPresenter;

public class MyProfileViewActivity extends BaseActivity implements MyProfileViewPresenter.View {

    @Inject
    MyProfileViewPresenter presenter;

    @BindView(R.id.profileImage) ImageView profileImage;
    @BindView(R.id.nameText) TextView name;
    @BindView(R.id.descText) TextView description;
    @BindView(R.id.retiredText) TextView retired;
    @BindView(R.id.modifyBtn) Button modify;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeDagger();
        showLoading();
        initializePresenter();
        setTitle("My Profile");

        modify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onModifyClick();
            }
        });
    }

    @Override public int getLayoutId() {
        return R.layout.my_profile_view;
    }

    @Override public void showUser(User user) {
        // Show image
        if (!(user.getImagePath()==null)) {
            Picasso.get().load(user.getImagePath()).fit().centerCrop().into(profileImage);
        }

        // Show name
        if (user.getFirstName()!=null && user.getLastName() != null) {
            name.setText(user.getFirstName().concat(" ").concat(user.getLastName()));
        } else if (user.getFirstName() != null) {
            name.setText(user.getFirstName());
        }
        else {
            name.setText(getString(R.string.no_name));
        }

        // Show description
        if (!(user.getDescription()==null)) description.setText(user.getDescription());
        else description.setText(getString(R.string.has_no_desc));

        // Show type user
        String ret;
        if (user.getElder()) ret = getString(R.string.user_type_bidding);
        else ret = getString(R.string.user_type_normal);
        retired.setText(ret);

    }

    public void onModifyClick() {
        ProfileModifyActivity.open(this);
    }

    @Override
    public void onSuccess() {
        Toast.makeText(this,"Requested Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailure() {
        Toast.makeText(this,"Requested Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void message(String str) {
        Toast.makeText(this,str, Toast.LENGTH_SHORT).show();
    }

    public static void open(Context context) {
        Intent intent = new Intent(context, MyProfileViewActivity.class);
        context.startActivity(intent);
    }

    private void initializeDagger() {
        KnitterApplication app = (KnitterApplication) getApplication();
        app.getMainComponent().inject(this);
    }

    private void initializePresenter() {
        presenter.setView(this);
        hideLoading();
        presenter.initialize();
    }
}