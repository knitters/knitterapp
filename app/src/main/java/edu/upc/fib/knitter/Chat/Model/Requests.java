package edu.upc.fib.knitter.Chat.Model;

public class Requests {

    public String name;
    public String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Requests(){}

    public Requests(String name, String description){
        this.name = name;
        this.description = description;
    }
}
