package edu.upc.fib.knitter.usecase;

import javax.inject.Inject;

import edu.upc.fib.knitter.apiclient.ApiRepository;
import edu.upc.fib.knitter.apiclient.model.MessageResponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Response;

public class ModifyUser {

    private final ApiRepository repository;

    @Inject
    public ModifyUser(ApiRepository repository) {
        this.repository = repository;
    }

    public void updateUser(final RequestBody firstName, final RequestBody lastName, final RequestBody description, final MultipartBody.Part image, final Callback callback) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Call<MessageResponse> response = repository.updateUser(firstName, lastName, description, image);
                response.enqueue(new retrofit2.Callback<MessageResponse>() {
                    @Override
                    public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                        callback.onResponse(call, response);
                    }

                    @Override
                    public void onFailure(Call<MessageResponse> call, Throwable t) {
                        callback.onFailure(call, t);
                    }
                });
            }
        }).start();
    }

    public interface Callback {
        void onResponse(Call<MessageResponse> call, Response<MessageResponse> response);

        void onFailure(Call<MessageResponse> call, Throwable t);
    }
}
